<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paymethodpayrun extends Model
{
    public $fillable = ['Name', 'Description','lock','active'];
}
