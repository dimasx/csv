<?php

namespace App\Http\Controllers;

use App\productos;
use App\employee;
use App\CsvData;
use App\Http\Requests\CsvImportRequest;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\config\app;
use PDF;
use DB;
class ImportController extends Controller
{

    public function parseImport(CsvImportRequest $request)
    {
        config([
            'config/app.php',
        ]);

        $path = $request->file('csv_file')->getRealPath();
        //si el archivo tiene encabezado esta es la opcion recomendada para trabajar//
        if ($request->has('header')) {
            Excel::setDelimiter(';'); //aca se configura el eparador 
            $data = Excel::load($path, function($reader) { })->get()->toArray();
        } else {
            //print_r(str_replace(';' , ',' ,file($path)));
            $data = array_map('str_getcsv', str_replace(';' , ',' ,file($path)));
        }

        if (count($data) > 0) {
            if ($request->has('header')) {
                $csv_header_fields = [];
                foreach ($data[0] as $key => $value) {
                    $csv_header_fields[] = $key;
                }
            }
            $csv_data = array_slice($data, 0, 2);

            $csv_data_file = CsvData::create([
                'csv_filename' => $request->file('csv_file')->getClientOriginalName(),
                'csv_header' => $request->has('header'),
                'csv_data' => json_encode($data)
            ]);
        } else {
            return redirect()->back();
        }

        return view('import_fields', compact( 'csv_header_fields', 'csv_data', 'csv_data_file'));

    }

    public function processImport(Request $request)
    {    
        
        $data = CsvData::find($request->csv_data_file_id);
        $csv_data = json_decode($data->csv_data, true);
        //print_r($csv_data);
        $nr=1;
        $mensaje[0]='Datos Importados Correctamente';
        foreach ($csv_data as $row) {
            $producto = new employee();
            $i=0;
            
            try {
                foreach ($request->fields as $index => $field) {

                    if ($data->csv_header) {
                        $fil=config('app.db_fields')[$i];
                        $producto->$fil = $row[$index];
                        $i++;
                    } else {
                        $fil=config('app.db_fields')[$index];
                        $producto->$fil = $row[$request->fields[$index]];
                    }
                    
                }
                $producto->save();
            } catch (\Exception $e) {
                if ($data->csv_header) {
                    $nombre=$row['firstname'];
                    $dni=$row['dni'];
                }else{
                    $nombre=$row[1];
                    $dni=$row[4];
                }
                if ($nr==1) {
                    $mensaje[0]='Datos Importados pero ocurrieron los siguientes errores:';
                }
                switch ($e->getCode()) {
                    case '0':
                        $mensaje[$nr]='La cantidad de campos del registro N° '.$nr.' nombre: '.$nombre.'  dni: '.$dni.' no son correctas';
                        break;
                    case 'HY000':
                        $mensaje[$nr]=' <br>Faltan campos en el registro N° '.$nr.' nombre: '.$nombre.'  dni: '.$dni;
                        break;
                    case '23000':
                        $mensaje[$nr]='El registros N° '.$nr.' nombre: '.$nombre.'  dni: '.$dni.' esta duplicado';
                        break;
                    default:
                        $mensaje[$nr]='El formato de los campos del registro N° '.$nr.' nombre: '.$nombre.'  dni: '.$dni.' no son correcto';
                        break;
                }
            }
            $nr++;
        }
        return view('import_success',compact('mensaje','nr'));
        
    }
     public function processExport()
    {

        Excel::create('Laravel Excel', function($excel) {
 
            $excel->sheet('Productos', function($sheet) {
 
                $products = Productos::all();
 
                //$sheet->fromArray($products);
                $sheet->loadView('exportXLS', compact('products'));
 
            });
        })->export('xls');
    }
    public function processExportPDF(){
  # $products = Productos::all();
      $idnemployee=1;
      $head=DB::table('payrunhead')          
                ->join('period', 'period.idn', '=', 'idnperiod')
                ->join('runtype', 'payrunhead.typerun', '=', 'runtype.idn')
                ->join('roster', 'roster.idn', '=', 'idnroster')  
                ->join('employee', 'employee.idn', '=', 'payrunhead.idnemployee')
                ->join('employeejobpositions','employeejobpositions.idnemployee','=','payrunhead.idnemployee')
                ->join('categorysalary','categorysalary.idn','=','employeejobpositions.idncategorysalary')
                ->join('positionjob','positionjob.idn','=','employeejobpositions.idnpositionjob')
                ->join('workplaces', 'workplaces.idn', '=', 'employee.idnworkplace')
                ->join('modcontract', 'modcontract.idn', '=', 'employee.idnmodcontract')
                ->join('paymethodpayrun', 'paymethodpayrun.idn', '=', 'idnpaymentmethod') 
                ->select('payrunhead.idn','payrunhead.idnemployee',DB::raw("CONCAT(employee.firstname,' ',employee.lastname)  AS nameemployee"),'employee.cuil',
                      'employeejobpositions.startdate','categorysalary.name as categorysalary','categorysalary.salaryamount','positionjob.name as namepositionjob',
                      'workplaces.name as nameworkplaces',DB::raw("CONCAT(modcontract.name,' ',modcontract.description) as nemamodcontract"),
                      'payrunhead.idnroster','roster.name as nameroster',
                      'payrunhead.idnperiod','period.title as titleperiod','payrunhead.typerun as idntyperun','runtype.name as nametyperun',
                      'payrunhead.idnpaymentmethod','paymethodpayrun.name as paymethodname','payrunhead.paydate','payrunhead.paycontributiondate',
                     //PAGOS
                     'payrunhead.payments','payrunhead.deductions','payrunhead.unremuns','payrunhead.exempts','payrunhead.status','payrunhead.totalpay',
                     'payrunhead.comment','payrunhead.idnemployee as legajo')               
            ->where('payrunhead.active',1)
            ->where('payrunhead.status',1)
            ->where('payrunhead.idnemployee',$idnemployee)
            ->get();

      $body=DB::table('payrunbody')
              ->join('concept', 'concept.idn', '=', 'idnconcept')
              ->select('payrunbody.idn','payrunbody.idnpayrunhead','payrunbody.idnconcept','concept.cod as codconcept','concept.idncolumn as columnconcept',
                     'concept.idncondition as conditionconcept','concept.name as nameconcept','payrunbody.cant','payrunbody.amount')
            ->where('payrunbody.active',1)
            ->where('payrunbody.idnpayrunhead',$head[0]->idn)
            ->get();
   

    $pdf = PDF::loadView('exportPDF1', compact('head','body','sueldo_r','sueldo','sueldo_remun_s','aporte','aporte_r','aporte_desc') );

    return $pdf->stream($head[0]->cuil."_".$head[0]->titleperiod.'.pdf');
    #return $pdf->stream('Recibo.pdf');
        
   
   // return view('exportPDF1', compact('head','body'));
    // "arielcr/numero-a-letras": "dev-master"
    }
    public function nacionalidad($value){
        /*Mi recomendacion con respecto a la nacionalidad es agregar a la tabla una columna con las siglas para hacer una busqueda mas facil*/
        $nac=4;
        switch ($value) {
                        case 'ARG':
                            $nac=4;
                            break;
                        case 'BOL':
                            $nac=9;
                            break;
                        case 'PAR':
                            $nac=43;
                            break;
                        /*agregar los caso de cada pais solo agregre los que se encontraba en la tablas*/
                        default:
                            $nac=4;
                            break;
                    }
        return $nac;
    }
    public function estadocivil($value){
        $edoc=4;
        switch ($value) {
                        case 'SI':
                            $edoc=1;
                            break;
                        case 'MA':
                            $edoc=2;
                            break;
                        case 'DI':
                            $edoc=3;
                            break;
                        case 'WI':
                            $edoc=4;
                            break;
                        case 'SE':
                            $edoc=5;
                            break;
                        case 'CO':
                            $edoc=6;
                            break;
                        default:
                            $edoc=1;
                            break;
                    }
        return $edoc;
    }
    public function razonSalida($value){
        $rs=1;
        switch ($value) {
                        case 'DIS':
                            $rs=3;
                            break;
                        case 'DWH':
                            $rs=2;
                            break;
                        /*agregar los caso de cada razon solo agregre los que se encontraba en la tablas*/
                        default:
                            $rs=1;
                            break;
                    }
        return $rs;
    }
    public function employeeImport(){
        $employee=DB::connection('dyktel')
                        ->table('personprofile')
                        ->join('person','id','=','personId')
                        ->select('personId','firstname','lastname','gender','dni','cuil','birthdate',DB::raw("'Nofoto' as photo"),'state',
                            DB::raw("'N/D' as province"),'zip','street','number','room','floor','phone','mobile','workemail','personalemail',
                            'nationality','maritalstatus','educationlevel','costcenter','department','position','workcalendar',DB::raw("'1' as idncct"),
                            DB::raw("'1' as idncategorysalary"),'terminationreason',DB::raw("'1' as idnsocialwork"),DB::raw("'1' as idncompany"),
                            DB::raw("'1' as idnsitrevision"),'contracttype','workplace',DB::raw("'1' as idnroster"),DB::raw("'1' as idnsindicate"),
                            DB::raw("'1' as idnregprevisional"),'sinistercode','condition','medicalplan','salary',DB::raw("'1' as employeestatus"),'startdate',
                            'startdaterecognized','terminationdate',DB::raw("'1' as antiquity"),'adherents',DB::raw("'1' as numberofcap"),
                            DB::raw("'1' as affiliate"),DB::raw("'1' as lifesecure"),DB::raw("'1' as benefitsoccharges"),'workagreements',
                            'person.lock','status')
                        ->get();
        $ce=$employee->count();
        $nr=1;
        $mensaje[0]='Datos Importados Correctamente';
        foreach ($employee as $row) {
            try {
                $emp = new employee();
                $j=0;
                foreach ($row as $key => $value) {
                    $col=config('app.db_fields')[$j];
                    switch ($j) {
                        case 3: 
                            if ($value=='M') {
                            $emp->$col=1;
                            }else{
                                $emp->$col=0;
                            }
                            break;
                        case 19:
                            $emp->$col=$this->nacionalidad($value);
                            break;
                        case 20:
                            $emp->$col=$this->estadocivil($value);
                            break;
                        case 21:
                            $emp->$col=1;//esto es el nivel de estudio. hay que averiguar los corespondiente a SCF y SEC
                            break;
                        case 25:
                            $emp->$col=1;//esto es el workcalendar no tengo la tabla maestro llena para realizar una comparacion 
                            break;
                        case 28:
                            $emp->$col=$this->razonSalida($value);//esto es razon de salidad este campo deberia aceptar null
                            break;
                        case 33:
                            $emp->$col=1;//esto es workplaces todos tenian WP01 
                            break;
                        default:
                            if ($value==null && in_array($j,[10,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,46,51])) {//aca deben ir todos los campos id que no aceptan null
                                $emp->$col=1;    
                            }elseif ($value==null && in_array($j,[11,12,13,14,15,16,17,18])) {//aca deben ir todod los campos tipo string que no aceptan null
                                $emp->$col='';
                            }elseif ($value==null && in_array($j,[6,42,43,44])) {//aca deben ir todos los campos tipo fecha que no aceptan null
                                $emp->$col='1970-01-01';
                            }
                            else
                                $emp->$col=$value;
                            break;
                    }
                        
                 // print_r(config('app.db_fields')[$j].':'.$value.' - ');
                  $j++;
                }
                $emp->save();
            } catch (\Exception $e) {
                if ($nr==1) {
                        $mensaje[0]='Datos Importados pero ocurrieron los siguientes errores:';
                    }
                switch ($e->getCode()) {
                        case '23000':
                            $mensaje[$nr]='El Empleado con Codigo '.$row->personId.' nombre: '.$row->firstname.'  dni: '.$row->dni.' esta duplicado';
                            break;
                        default:
                            $mensaje[$nr]='El Empleado con Codigo '.$row->personId.' nombre: '.$row->firstname.'  dni: '.$row->dni.' no fue importado';
                            break;
                    }
                    $nr++;
            }
        }
        return view('import_employee',compact('ce','nr','mensaje'));
    }
}
