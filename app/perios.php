<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class period extends Model
{
    public $fillable = ['title', 'idncalendar', 'idntypeperiod','startdate','finishdate','year','month','sicossactive','special','numperiod','lock','active'];
}
