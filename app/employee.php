<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
	protected $table = 'employee';
    public $fillable = ['cod', 'firstname', 'lastname','gender','dni','cuil','birthdate','photo','city','province','postalcode','street','number','floor','department','telephone','cellphone','workemail','personmail','idnnationality','idncivilstatus','idnlevelstudies','idncostcenter','idndepartment','idnpositionjob','idnworkcalendar','idncct','idncategorysalary','idnexitreason','idnsocialwork','idncompany','idnsitrevision','idnmodcontract','idnworkplace','idnroster','idnsindicate','idnregprevisional','idnsinestercode','idnemployeecondition','idnmedicalplan','basicsalary','employeestatus','dateentry','dateentryrecognized','dateexit','antiquity','adherents','numberofcap','affiliate','lifesecure','benefitsoccharges','agreed','lock','active'];
}
