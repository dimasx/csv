<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class roster extends Model
{
    public $fillable = ['Name', 'Description','idnvacationtype','idncalendar','	idnsalarytype','numperiodmonth','journaltype','journalteoric','lock','active'];
}
