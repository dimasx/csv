<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class runtype extends Model
{
    public $fillable = ['Name', 'Description','lock','active'];
}
