<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payrunhead extends Model
{
    public $fillable = ['idnperiod', 'typerun', 'idnemployee','idnroster','idnpaymentmethod','paydate','paycontributiondate','payments','deductions','unremuns','exempts','totalpay','status','comment','lock','active'];
}
