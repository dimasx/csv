<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('importar');
});
Route::post('/import_parse', 'ImportController@parseImport')->name('import_parse');
Route::post('/import_process', 'ImportController@processImport')->name('import_process');
Route::get('/export_process', 'ImportController@processExport')->name('export_process');
Route::get('/exportPDF_process', 'ImportController@processExportPDF')->name('Previsualización');
Route::get('/import_employee', 'ImportController@employeeImport')->name('import_employee');