@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Importar desde CSV</div>

                    <div class="panel-body">
                        Cantidad de registros procesados {{$nr-1}} <br>
                        Cantidad de registros importados con exito {{$nr-count($mensaje)}} <br>
                        <p> 
                            @foreach ($mensaje as $linea)
                                {{$linea}}
                                <br>
                            @endforeach
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
