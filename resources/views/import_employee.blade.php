@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Importar desde bd</div>

                    <div class="panel-body">
                        Cantidad de registros procesados {{$ce}} <br>
                        Cantidad de registros importados con exito {{$ce-$nr+1}} <br>
                        <p> 
                            @foreach ($mensaje as $linea)
                                {{$linea}}
                                <br>
                            @endforeach
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
