<!DOCTYPE html>
<html >
<head>
    <meta charset="utf-8">

    <!-- Styles -->
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <table style="width:100%" id="Reporte" class="quitar-abajo">
  <tr>
    <th class="titulo padding_left padding_bot padding_top" colspan="4">PURO PRESENTA, SA</th>
    <th colspan="3" class="cuit titulo padding_left padding_bot padding_top">CUIT: 30-71182465-7</th> 
  </tr>

  <tr>
    <td colspan="7" class="padding_left padding_bot padding_top">Paraguay 390, Piso 5. CABA</td>
  </tr>


  <tr class="centrar negrita">
    <td colspan="4" >APELLIDO Y NOMBRE</td>
    <td colspan="1"  >CUIL </td>
    <td colspan="2" >LEGAJO</td>
  </tr>

  <tr class="centrar " >
    <td colspan="4" class="nombres">{{$head[0]->nameemployee}}</td>
    <td colspan="1" class="nombres" style="width: 150px;">{{$head[0]->cuil}}</td>
    <td colspan="2" class="nombres">{{$head[0]->legajo}}</td>
  </tr>
  
  <tr >
    <td colspan="4" rowspan="2"   class="quitar-superior quitar-abajo padding_left padding_top-1 padding_bot-1" rowspan="2" > <span class="negrita"> CATEGORIA</span> <div  valign="top " > {{$head[0]->categorysalary}} </div></td>
    <td colspan="1" class="negrita padding_left" style="">FECHA DE <br>  INGRESO </td>
    <td colspan="2" class="negrita padding_left padding_top" >REMUNERACION <br>   ASIGNADA</td>

  </tr>

   <tr >
    <td colspan="1" class="centrar">{{$head[0]->startdate}}  </td>
    <td colspan="2" class="centrar">$ {{number_format($head[0]->salaryamount, 2, ",", ".")}}</td>
  </tr>

   <tr   >
    <td colspan="4" class="quitar-abajo quitar-superior  negrita padding_left padding_top-1 padding_bot-1" valign="top" >CALIF. PROFESIONAL </td>
    <td colspan="3" class="padding_left padding_top"> <strong >PERIODO DE PAGO:</strong> {{$head[0]->titleperiod}}  </td>
  </tr>

  <tr >
    <td colspan="4" class="quitar-superior quitar-abajo padding_left  padding_top-0 " valign="top" >{{$head[0]->namepositionjob}}    </td>
    <td colspan="3" class=" quitar-abajo negrita padding_left padding_top" valign="top">CONTRATACION </td>
  </tr>
   <tr >
    <td colspan="4" class="quitar-superior quitar-abajo negrita padding_left padding_top" valign="">LUGAR DE TRABAJO  </td>
    <td colspan="3" rowspan="2"  class="quitar-superior quitar-abajo padding_left"  valign="top" height="30">{{$head[0]->nemamodcontract}}</td>
  </tr>
   <tr >
    <td colspan="4" class="quitar-superior quitar-abajo padding_left padding_bot" valign="top ">{{$head[0]->nameworkplaces}} <br>  </td>
  </tr>
</table>
<table>
  <tr>
    <td colspan="2" class="centrar quitar-superior padding_top" style="width:200px;"><strong>CONCEPTO</strong></td>
    <td  class="centrar quitar-superior  padding_top" colspan="1" style="width: 60px;"> <strong>CANT</strong></td>
    <td  class="centrar quitar-superior  padding_top" colspan="1" style="width: 100px;"> <strong>REMUN.SUJ.A RET.</strong></td>
    <td  class="centrar quitar-superior  padding_top" colspan="1"  style="width: 100px;"> <strong>NO <br>REMUNERAT.</strong></td>
    <td  class="centrar quitar-superior  padding_top" colspan="1" style="width: 100px;"> <strong>REMUN. EXENTAS</strong></td>
    <td  class="centrar quitar-superior  padding_top" colspan="1" style="width: 100px;"> <strong>DESCUENTOS </strong></td>

  </tr>
@foreach ($body as $row)
  <tr>
    <td style="height: 23px;" colspan="2" class="quitar-abajo quitar-superior">{{$row->nameconcept}}</td>
    <td  class="centrar quitar-abajo quitar-superior"> 
     {{number_format($row->cant, 2, ",", ".")}}
    </td>
    <td  class="centrar quitar-abajo quitar-superior">  
      @if($row->columnconcept==1) 
          {{number_format($row->amount, 2, ",", ".")}}
      @endif
    </td>
    <td  class="centrar quitar-abajo quitar-superior">
      @if($row->columnconcept==2) 
          {{number_format($row->amount, 2, ",", ".")}}
      @endif
    </td>
    <td  class="centrar quitar-abajo quitar-superior">
      @if($row->columnconcept==3) 
          {{number_format($row->amount, 2, ",", ".")}}
      @endif
    </td>
    <td  class="centrar quitar-abajo quitar-superior">
      @if($row->columnconcept==4) 
          {{number_format($row->amount, 2, ",", ".")}}
      @endif
    </td>
  </tr>
@endforeach
@for ($i = count($body); $i < 15; $i++)
  <tr >
    <td  style="height: 23px;" colspan="2" class="quitar-abajo quitar-superior"></td>
    <td  class="centrar quitar-abajo quitar-superior"></td>
    <td  class="centrar quitar-abajo quitar-superior"></td>
    <td  class="centrar quitar-abajo quitar-superior"></td>
    <td  class="centrar quitar-abajo quitar-superior"></td>
    <td  class="centrar quitar-abajo quitar-superior"></td>
  </tr>
@endfor 



   <tr class="negrita">
    <td colspan="3" class="quitar-abajo padding_left"></td>
    <td  class="centrar quitar-abajo padding_left">{{number_format($head[0]->payments, 2, ",", ".")}} </td>
    <td  class="centrar quitar-abajo padding_left">{{number_format($head[0]->unremuns, 2, ",", ".")}}</td>
    <td  class="centrar quitar-abajo padding_left">{{number_format($head[0]->exempts, 2, ",", ".")}}</td>
    <td  class="centrar quitar-abajo padding_left">{{number_format($head[0]->deductions, 2, ",", ".")}}</td>
  </tr>

  <tr>
    <td colspan="4" class="quitar-abajo negrita  padding_left">LUGAR Y FECHA DE PAGO</td>
    <td colspan="2" class="quitar-abajo  padding_left">TOTAL NETO</td>
    <td colspan="1" rowspan="2" class="quitar-abajo centrar negrita padding_left"> {{number_format($head[0]->totalpay, 2, ",", ".")}}</td>

  </tr>

  <tr>
    <td colspan="4" class="quitar-abajo quitar-superior  padding_lef padding_bot">Paraguay 390, Piso 5. CABA, 05/01/2018</td>
    <td colspan="2" class="quitar-abajo quitar-superior padding_left">FORMA DE PAGO</td>

  </tr>

  <tr>
    <td colspan="7" class="quitar-abajo padding_left"><strong>SON PESOS:</strong></td>
    

  </tr>
   <tr>
    <td colspan="7" class="quitar-abajo quitar-superior padding_left">NUEVE MIL</td>
    

  </tr>

  <tr>
    <td style="height: 70px; " colspan="7" class="quitar-abajo  negrita padding_left">OBSERVACIONES:<br>
    <p class="">
      {{$head[0]->comment}}
    </p>
    </td>
    

  </tr>


  <tr>
    <td colspan="2" class="negrita " style="font-size: 10px; margin-left: 50px;">
<p style="margin-left: 5px;">ART. 12 LEY 17.250 <br>
MES: Noviembre <br>
BANCO: Red Link <br>
FECHA DE DEPOSITO: <br>
13/12/2017</td>
</p>
    <td colspan="3" class="centrar negrita" style="font-size: 10px;">RECIBI EL IMPORTE NETO DE ESTA LIQUIDACION EN PAGO  DE MI REMUNERACION CORRESPONDIENTE AL PERIODO INDICADO Y DUPLICADO DE LA MISMA CONFORME A LA LEY VIGENTE.</td>
    <td colspan="2"  class="centrar negrita " align="right" valign="bottom">FIRMA DEL EMPLEADO</td>

  </tr>







</table>

</body>
</html>
