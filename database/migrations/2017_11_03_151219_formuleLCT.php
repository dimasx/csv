<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FormuleLCT extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
            //Crear Tabla
            Schema::create('formulelct', function (Blueprint $table) {
            //Autoincrementable
            $table->increments('idn');
            $table->string('calc');
            $table->integer('idnconcept');
            //Datos por default en todas las tablas
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);  
            $table->timestamps();
        });
               DB::table('formulelct')
                        ->insert(array( 'calc' => '200+250',
                                        'idnconcept'=>1
                                       ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('formulelct');
    }
}
