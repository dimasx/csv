<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeHoursWorked extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('employeehoursworked', function (Blueprint $table) {
            $table->increments('idn');
            $table->date('startdate');
            $table->date('finishdate');
            $table->time('starthour');
            $table->time('finishhour');
            $table->string('observation');
            $table->integer('idnemployee');
            $table->integer('cant');
            $table->integer('lock')->default(1); 
            $table->integer('active')->default(1);         
            $table->timestamps();
        });
            DB::table('employeehoursworked')
                        ->insert(array(
                            'startdate' => '2017-12-12',
                            'finishdate' => '2018-08-12',
                            'starthour' => '11:20',
                            'finishhour' => '12:32',
                            'observation' => 'horas extras noviembre',
                            'idnemployee' => 1,
                            'cant' => 3
                            ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('employeehoursworked');
    }
}
