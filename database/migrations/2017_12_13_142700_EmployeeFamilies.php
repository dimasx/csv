<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeFamilies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          //Crear Tabla
            Schema::create('employeefamilies', function (Blueprint $table) {
            //Autoincrementable
            $table->increments('idn');       
            $table->string('typefamily');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('cuil');
            $table->date('birthdate');
            $table->string('gender');
            $table->integer('idnemployee');
            //Datos por default en todas las tablas
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);  
            $table->timestamps();
        });
               DB::table('employeefamilies')
                        ->insert(array( 
                            'typefamily' => 'hijo',
                            'firstname' => 'Juan',
                            'lastname' => 'Perez',
                            'cuil' => '12312312',
                            'birthdate' => '2017/11/10',
                            'gender'=>'M',
                            'idnemployee' => 1   
                                       ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('employeefamilies');
    }
}
