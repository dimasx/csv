<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Audit extends Migration
{
      /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('audit', function (Blueprint $table) {
            $table->increments('idn');
            $table->integer('idnuser');
              $table->string('accion');
              $table->string('window');
            $table->string('description');
                $table->timestamps();
        });
            DB::table('audit')
                        ->insert(array(
                            'idnuser' => '1',
                            'accion' => 'guardar',
                            'window' => 'employee',
                            'description' => 'guardo un legajo'
                            ));
                      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('audit');
    }
}
