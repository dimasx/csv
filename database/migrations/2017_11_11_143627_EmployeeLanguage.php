<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeLanguage extends Migration
{
       /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
            //Crear Tabla
            Schema::create('employeelanguages', function (Blueprint $table) {
            //Autoincrementable
            $table->increments('idn');       
            $table->integer('idnlanguage');
            $table->integer('level');
            $table->integer('idnemployee');
            //Datos por default en todas las tablas
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);  
            $table->timestamps();
        });
               DB::table('employeelanguages')
                        ->insert(array( 
                            'idnlanguage' => 1,
                            'level'=>1,
                            'idnemployee'=>1                                
                                       ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('employeelanguages');
    }
}
