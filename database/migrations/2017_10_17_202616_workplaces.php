<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Workplaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('workplaces', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

         DB::table('workplaces')
                ->insert(array('name' => 'Lugar de trabajo 01',
                            'description' => 'Lugar de trabajo 01'));
        DB::table('workplaces')
                ->insert(array('name' => ' Lugar de trabajo 02',
                            'description' => 'Lugar de trabajo 02'));
        DB::table('workplaces')
                ->insert(array('name' => 'xxxx',
                            'description' => 'xxxx'));

               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workplaces');
    }
}
