<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeAbsences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('employeeabsences', function (Blueprint $table) {
            $table->increments('idn');
            $table->integer('idnabsencetype');
            $table->date('startdate');
            $table->date('finishdate');
            //$table->string('observation');
            $table->integer('idnemployee');
            $table->integer('cant');
            $table->integer('lock')->default(1); 
            $table->integer('active')->default(1);         
            $table->timestamps();
        });
            DB::table('employeeabsences')
                        ->insert(array(
                            'idnabsencetype' => 1,
                            'startdate' => '2017-08-01',
                            'finishdate' => '2017-12-12',
                            //'observation' => 'nueva ausencia',
                            'idnemployee' => 1,
                            'cant' => 3
                         
                            ));
           DB::table('employeeabsences')
                        ->insert(array(
                            'idnabsencetype' => 1,
                            'startdate' => '2017-08-01',
                            'finishdate' => '2017-12-12',
                            //'observation' => 'nueva ausencia2',
                            'idnemployee' => 1,
                            'cant' => 3
                         
                            ));
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('employeeabsences');
    }
}
