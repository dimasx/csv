<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Payrunhead extends Migration
{
   
    
     public function up()
    {
        Schema::create('payrunhead', function (Blueprint $table) {
            $table->increments('idn');
            $table->integer('idnperiod');
            $table->integer('typerun');
            $table->integer('idnemployee');
            $table->integer('idnroster');
            $table->integer('idnpaymentmethod');
            $table->date('paydate');
            $table->date('paycontributiondate');
            //PAGOS
            $table->float('payments');
            $table->float('deductions');
            $table->float('unremuns');
            $table->float('exempts');
            $table->float('totalpay');
            //STATUS
            $table->integer('status');
            $table->string('comment');
            
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

        DB::table('payrunhead')
                ->insert(array('idnperiod' => '1',
                               'idnemployee' => '1',
                               'typerun' => '1',
                               'idnroster' => '1',
                               'idnpaymentmethod' => '1',
                               'paydate' => '2017-05-05',
                               'paycontributiondate' => '2017-04-04',
                               //PAGOS
                               'payments' => '15671',
                               'deductions' => '1234',
                               'unremuns' => '2345',
                               'exempts' => '134',
                               'totalpay' => '500',
                               //STATUS
                               'status' => '1',
                               'comment' => 'prueba'
                              ));

               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrunhead');
    }
    
    
    
    
    
    
}
