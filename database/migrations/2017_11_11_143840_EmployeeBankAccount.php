<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeBankAccount extends Migration
{
         /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
            //Crear Tabla
            Schema::create('employeebankaccounts', function (Blueprint $table) {
            //Autoincrementable
            $table->increments('idn'); 
            $table->integer('idnpaymentmethod');          
            $table->integer('idnbank');
            $table->string('numberaccount');
            $table->integer('idnemployee');
            //Datos por default en todas las tablas
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);  
            $table->timestamps();
        });
               DB::table('employeebankaccounts')
                        ->insert(array( 
                            'idnpaymentmethod' => 1,                        
                            'idnbank' => 1,
                            'numberaccount' => '12334234',
                            'idnemployee' => 1
                                                     
                                       ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('employeebankaccounts');
    }
}
