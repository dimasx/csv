<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sindicate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sindicate', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });
        DB::table('sindicate')
                ->insert(array('name' => 'AERONAUTICO',
                            'description' => 'AERONAUTICO'));
                DB::table('sindicate')
                ->insert(array('name' => 'AERONAVEGACION',
                            'description' => 'AERONAVEGACION'));
                DB::table('sindicate')
                ->insert(array('name' => 'UOCRA',
                            'description' => 'UOCRA'));
                DB::table('sindicate')
                ->insert(array('name' => 'PASTAS FRESCAS',
                            'description' => 'PASTAS FRESCAS'));
                DB::table('sindicate')
                ->insert(array('name' => 'CAUCHO',
                            'description' => ' CAUCHO'));
                DB::table('sindicate')
                ->insert(array('name' => 'COMERCIO',
                            'description' => 'COMERCIO'));
                DB::table('sindicate')
                ->insert(array('name' => 'MAESTRANZA',
                            'description' => 'MAESTRANZA'));
                DB::table('sindicate')
                ->insert(array('name' => 'UTHGRA',
                            'description' => 'UTHGRA'));
                DB::table('sindicate')
                ->insert(array('name' => 'SIN SINDICATO',
                            'description' => 'SIN SINDICATO'));
                DB::table('sindicate')
                ->insert(array('name' => 'Sanidad',
                            'description' => 'Sanidad'));

               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sindicate');
    }
}
