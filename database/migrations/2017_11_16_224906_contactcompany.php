<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contactcompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactcompany', function (Blueprint $table) {
            $table->increments('idn');
            $table->integer('idncompany');
            $table->string('name')->unique();
            $table->string('email');
            $table->string('telephone');
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);
            $table->timestamps();
        });

         DB::table('contactcompany')
                ->insert(array('idncompany' => 1,
                               'name' => 'Carmen Perez',
                               'email' => 'CarmenP@gmail.com',
                               'telephone' => '04165555555'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contactcompany');
    }
}
