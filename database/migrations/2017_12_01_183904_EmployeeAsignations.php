<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeAsignations extends Migration
{
      /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
          Schema::create('employeeasignations', function (Blueprint $table) {
            $table->increments('idn');
            $table->integer('idnperiod');
            $table->integer('idnsubperiod');
            $table->integer('idnconcept');
            $table->float('amount');
            $table->date('startdate');
            $table->date('finishdate');
            $table->integer('cant');
            $table->integer('specialtype');
            $table->integer('idnemployee')->default(1);
            $table->string('description');
            $table->integer('lock')->default(1); 
            $table->integer('active')->default(1);         
            $table->timestamps();
        });
            DB::table('employeeasignations')
                        ->insert(array(
                            'idnperiod' => 1,
                            'idnsubperiod' => 1,
                            'idnconcept' => 1,
                            'amount' => 2367,
                            'startdate' => '2017-08-01',
                            'finishdate' => '2017-12-12',
                            'cant' => 12,
                            'specialtype' => 1,
                            'idnemployee' => 1,
                            'description' => 'nueva deducción'
                            ));
          DB::table('employeeasignations')
                        ->insert(array(
                            'idnperiod' => 1,
                            'idnsubperiod' => 1,
                            'idnconcept' => 1,
                            'amount' => 236734,
                            'startdate' => '2017-12-12',
                            'finishdate' => '2018-08-12',
                            'cant' => 12,
                            'specialtype' => 1,
                            'idnemployee' => 1,
                            'description' => 'nueva asignación'
                         
                            ));
        
                      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('employeeasignations');
    }
}
