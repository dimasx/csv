<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Language extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('language', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

       DB::table('language')
                ->insert(array('name' => 'ingles',
                            'description' => 'idioma americano'));

               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('language');
    }
}
