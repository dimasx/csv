<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeCareers extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
            //Crear Tabla
            Schema::create('employeecareers', function (Blueprint $table) {
            //Autoincrementable
            $table->increments('idn');            
            $table->integer('idncareer'); 
            $table->string('title');
            $table->date('startdate');
            $table->date('finishdate');
            $table->integer('idnemployee');  
            //Datos por default en todas las tablas
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);  
            $table->timestamps();
        });
               DB::table('employeecareers')
                        ->insert(array( 
                            'idncareer' => 1,
                            'title' => 'Ingeniero',
                            'startdate'=>'2017/11/01',
                            'finishdate'=>'2017/11/01',
                            'idnemployee' => 1
                                       ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('employeecareers');
    }
}
