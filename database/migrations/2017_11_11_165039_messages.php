<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Messages extends Migration
{
      /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('messages', function (Blueprint $table) {
            $table->increments('idn');
            $table->integer('idncompany');
            $table->integer('idntransmitter');
            $table->integer('idnreceiver');
            $table->string('tittle');
            $table->string('description');
            $table->integer('lock')->default(1); 
            $table->integer('active')->default(1);         
            $table->timestamps();
        });
            DB::table('messages')
                        ->insert(array(
                            'idncompany' => '1',
                            'idntransmitter' => '1',
                            'idnreceiver' => '1',
                            'tittle' => 'tittle',
                            'description' => 'test'
                            ));
                      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('messages');
    }
}
