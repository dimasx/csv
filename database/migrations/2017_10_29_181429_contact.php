<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contact extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('email');
            $table->string('phone');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

        DB::table('contact')
                ->insert(array('name' => 'nestor',
                            'email' => 'colmenares203@gmail.com',
                               'phone' => '04145553095'));

               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact');
    }
}