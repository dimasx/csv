<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class payrunfooter extends Migration
{
   
    
     public function up()
    {
        Schema::create('payrunfooter', function (Blueprint $table) {
            $table->increments('idn');
            $table->integer('idnpayrunhead');
            $table->string('variable');
            $table->float('amount');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

        DB::table('payrunfooter')
                ->insert(array('idnpayrunhead' => '1',
                                'variable' => '1',
                               'amount' => '13231'
                               
                              ));

               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrunfooter');
    }
    
    
    
    
    
    
}
