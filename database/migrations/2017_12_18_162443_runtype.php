<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Runtype extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('runtype', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
            $table->integer('lock')->default(1); 
            $table->integer('active')->default(1);         
            $table->timestamps();
        });
            DB::table('runtype')
                        ->insert(array(
                            'name' => 'Default',
                            'description' => 'estandar'
                            ));
         DB::table('runtype')
                        ->insert(array(
                            'name' => 'Vacaciones en recibo separado',
                            'description' => 'vacaciones'
                            ));
         DB::table('runtype')
                        ->insert(array(
                            'name' => 'Sac en recibo separado',
                            'description' => 'sac separado'
                            ));
         DB::table('runtype')
                        ->insert(array(
                            'name' => 'Terminacion',
                            'description' => 'terminacion'
                            ));
         DB::table('runtype')
                        ->insert(array(
                            'name' => 'Anticipo vacaciones recibo separado',
                            'description' => 'anticipo vacaciones'
                            ));
         DB::table('runtype')
                        ->insert(array(
                            'name' => 'Acreditaciones adicionales ',
                            'description' => 'ajustes por errores anteriores'
                            ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('runtype');
    }
}
