<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Nationality extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nationality', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name');
            $table->string('description');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

         DB::table('nationality')
                ->insert(array('name' => 'Afgano',
                            'description' => 'Afganistán'));
         DB::table('nationality')
                ->insert(array('name' => 'Albanés',
                            'description' => 'República de Albania'));
         DB::table('nationality')
                ->insert(array('name' => 'Alemán',
                            'description' => 'Alemán'));
         DB::table('nationality')
                ->insert(array('name' => 'Argentino',
                            'description' => 'República Argentina'));
         DB::table('nationality')
                ->insert(array('name' => 'Armenio',
                            'description' => 'Armenia'));
         DB::table('nationality')
                ->insert(array('name' => 'Australiano',
                            'description' => 'Australia'));
         DB::table('nationality')
                ->insert(array('name' => 'Austriaco',
                            'description' => 'República de Austria Alemana'));
         DB::table('nationality')
                ->insert(array('name' => 'Belga',
                            'description' => 'Bélgica'));
         DB::table('nationality')
                ->insert(array('name' => 'Boliviano',
                            'description' => 'Bolivia'));
         DB::table('nationality')
                ->insert(array('name' => 'Brasilero',
                            'description' => 'República Federativa de Brasil​​​​'));
         DB::table('nationality')
                ->insert(array('name' => 'Búlgaro',
                            'description' => 'República de Bulgaria'));
         DB::table('nationality')
                ->insert(array('name' => 'Canadiense',
                            'description' => 'Canadá'));
         DB::table('nationality')
                ->insert(array('name' => 'Chileno',
                            'description' => 'República de Chile'));
         DB::table('nationality')
                ->insert(array('name' => 'Chino',
                            'description' => 'República Popular China'));
         DB::table('nationality')
                ->insert(array('name' => 'Colombiano',
                            'description' => 'República de Colombia'));
         DB::table('nationality')
                ->insert(array('name' => 'Coreano',
                            'description' => 'República de Corea'));
         DB::table('nationality')
                ->insert(array('name' => 'Costarricense',
                            'description' => 'República de Costa Rica'));
         DB::table('nationality')
                ->insert(array('name' => ' Croata',
                            'description' => 'República de Croacia'));
         DB::table('nationality')
                ->insert(array('name' => 'Cubano',
                            'description' => 'República de Cuba'));
         DB::table('nationality')
                ->insert(array('name' => 'Danés',
                            'description' => 'Dinamarca'));
         DB::table('nationality')
                ->insert(array('name' => 'Ecuatoriano',
                            'description' => 'República del Ecuador'));
         DB::table('nationality')
                ->insert(array('name' => 'Salvadoreño',
                            'description' => 'El Salvador'));
         DB::table('nationality')
                ->insert(array('name' => 'Español',
                            'description' => 'Reino de España'));
         DB::table('nationality')
                ->insert(array('name' => 'Estadounidense',
                            'description' => 'Estados Unidos de América'));
         DB::table('nationality')
                ->insert(array('name' => 'Filipino',
                            'description' => 'República de Filipinas'));
         DB::table('nationality')
                ->insert(array('name' => 'Finlandés',
                            'description' => 'República de Finlandia'));
         DB::table('nationality')
                ->insert(array('name' => 'Francés',
                            'description' => 'República Francesa '));
         DB::table('nationality')
                ->insert(array('name' => 'Guatemalteco',
                            'description' => 'República de Guatemala'));
         DB::table('nationality')
                ->insert(array('name' => 'Haitiano',
                            'description' => 'República de Haití'));
         DB::table('nationality')
                ->insert(array('name' => 'Hondureño',
                            'description' => 'República de Honduras'));
         DB::table('nationality')
                ->insert(array('name' => 'Iraquí',
                            'description' => 'República de Irak​'));
         DB::table('nationality')
                ->insert(array('name' => 'Iraní',
                            'description' => 'República Islámica de Irán'));
         DB::table('nationality')
                ->insert(array('name' => 'Irlandés',
                            'description' => 'República de Irlanda'));
         DB::table('nationality')
                ->insert(array('name' => 'Israelí',
                            'description' => 'Estado de Israel'));
         DB::table('nationality')
                ->insert(array('name' => 'Italiano',
                            'description' => 'República Italiana'));
         DB::table('nationality')
                ->insert(array('name' => 'Jamaiquino',
                            'description' => 'Jamaica'));
         DB::table('nationality')
                ->insert(array('name' => 'Japonés',
                            'description' => 'Estado del Japón'));
         DB::table('nationality')
                ->insert(array('name' => 'Libanés',
                            'description' => 'República Libanesa'));
         DB::table('nationality')
                ->insert(array('name' => 'Marroquí',
                            'description' => 'Marruecos'));
         DB::table('nationality')
                ->insert(array('name' => 'Mexicano',
                            'description' => 'Estados Unidos Mexicanos'));
         DB::table('nationality')
                ->insert(array('name' => 'Nicaragüense',
                            'description' => 'República de Nicaragua'));
         DB::table('nationality')
                ->insert(array('name' => 'Nigeriano',
                            'description' => 'República Federal de Nigeria'));
         DB::table('nationality')
                ->insert(array('name' => 'Paraguayo',
                            'description' => 'República del Paraguay'));
         DB::table('nationality')
                ->insert(array('name' => 'Peruano',
                            'description' => 'República del Perú'));
         DB::table('nationality')
                ->insert(array('name' => 'Portugués',
                            'description' => 'República Portuguesa'));
         DB::table('nationality')
                ->insert(array('name' => 'Británico',
                            'description' => 'Reino Unido'));
         DB::table('nationality')
                ->insert(array('name' => 'Dominicano',
                            'description' => 'República Dominicana'));
         DB::table('nationality')
                ->insert(array('name' => 'Sudafricano',
                            'description' => 'República de Sudáfrica'));
         DB::table('nationality')
                ->insert(array('name' => 'Ruso',
                            'description' => 'Federación Rusa'));
         DB::table('nationality')
                ->insert(array('name' => 'Sirio',
                            'description' => 'República Árabe Siria'));
         DB::table('nationality')
                ->insert(array('name' => 'Sueco',
                            'description' => 'Reino de Suecia'));
         DB::table('nationality')
                ->insert(array('name' => 'Suizo',
                            'description' => 'Reino de Suecia'));
         DB::table('nationality')
                ->insert(array('name' => 'Turco',
                            'description' => 'República de Turquía'));
         DB::table('nationality')
                ->insert(array('name' => 'Uruguayo',
                            'description' => 'República Oriental del Uruguay'));
         DB::table('nationality')
                ->insert(array('name' => 'Venezolano',
                            'description' => ' República Bolivariana de Venezuela'));


               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nationality');
    }
}
