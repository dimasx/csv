<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeHolidays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('employeeholidays', function (Blueprint $table) {
            $table->increments('idn');
            $table->date('startdate');
            $table->date('finishdate');
            $table->integer('cant');
            $table->string('description');
            $table->integer('idnemployee');
            $table->integer('lock')->default(1); 
            $table->integer('active')->default(1);         
            $table->timestamps();
        });
            DB::table('employeeholidays')
                        ->insert(array(
                            'startdate' => '2017-12-12',
                            'finishdate' => '2018-08-12',
                            'cant' => 3,
                            'description' => 'horas extras noviembre',
                            'idnemployee' => 1
                            ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employeeholidays');
    }
}
