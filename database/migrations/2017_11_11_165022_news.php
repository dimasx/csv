<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class News extends Migration
{
      /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('news', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('tittle');
            $table->string('description');
            $table->integer('lock')->default(1); 
            $table->integer('active')->default(1);         
            $table->timestamps();
        });
            DB::table('news')
                        ->insert(array(
                            'tittle' => 'test',
                            'description' => 'test'
                            ));
                      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('news');
    }
}
