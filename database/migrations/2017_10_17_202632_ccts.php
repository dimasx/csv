<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ccts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ccts', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });
               DB::table('ccts')
                ->insert(array('name' => 'C.C. de Comercio',
                            'description' => 'C.C. de Comercio'));
                DB::table('ccts')
                ->insert(array('name' => 'C.C. B',
                            'description' => 'C.C. B'));
                DB::table('ccts')
                ->insert(array('name' => 'Construcción',
                            'description' => 'Construcción'));
                DB::table('ccts')
                ->insert(array('name' => 'Fuera de Convenio',
                            'description' => 'Fuera de Convenio'));
                DB::table('ccts')
                ->insert(array('name' => 'Comercio',
                            'description' => 'Comercio'));
                DB::table('ccts')
                ->insert(array('name' => 'Caucho',
                            'description' => 'Caucho'));
                DB::table('ccts')
                ->insert(array('name' => 'UTHGRA',
                            'description' => 'UTHGRA'));
                DB::table('ccts')
                ->insert(array('name' => 'Maestranza',
                            'description' => 'Maestranza'));
                DB::table('ccts')
                ->insert(array('name' => 'Pastas Frescas',
                            'description' => 'Pastas Frescas'));
                DB::table('ccts')
                ->insert(array('name' => 'Farmacia',
                            'description' => 'Farmacia'));
                DB::table('ccts')
                ->insert(array('name' => 'Asociación Civil',
                            'description' => 'Asociación Civil'));
                DB::table('ccts')
                ->insert(array('name' => 'Sanidad',
                            'description' => 'Sanidad'));
                DB::table('ccts')
                ->insert(array('name' => 'Textil (501/07)',
                            'description' => 'Textil (501/07)'));
                DB::table('ccts')
                ->insert(array('name' => 'Camioneros',
                            'description' => 'Camioneros'));
                DB::table('ccts')
                ->insert(array('name' => 'Textiles Obreros (CCT 123/90)',
                            'description' => 'Textiles Obreros (CCT 123/90)'));
                DB::table('ccts')
                ->insert(array('name' => 'Químicos y Petroquímicos (CCT 77/89)',
                            'description' => 'Químicos y Petroquímicos (CCT 77/89)'));

               


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccts');
    }
}
