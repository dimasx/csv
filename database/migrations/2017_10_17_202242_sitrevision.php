<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sitrevision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('sitrevision', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

        DB::table('sitrevision')
                ->insert(array('name' => 'Baja por fallecimiento',
                            'description' => 'Baja por fallecimiento'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Activo',
                            'description' => 'Activo'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Bajas otras causales',
                            'description' => 'Bajas otras causales'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Activo Decreto N° 796/97',
                            'description' => 'Activo Decreto N° 796/97'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Bajas otras causales Decreto N° 796/97',
                            'description' => 'Bajas otras causales Decreto N° 796/97'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Licencia por maternidad',
                            'description' => 'Licencia por maternidad'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Suspensiones otras causales',
                            'description' => 'Suspensiones otras causales'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Baja por despido',
                            'description' => 'Baja por despido'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Baja por despido Decreto N° 796/97',
                            'description' => 'Baja por despido Decreto N° 796/97'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Suspendido. Art. 223 bis de la Ley N° 20.744.',
                            'description' => 'Suspendido. Art. 223 bis de la Ley N° 20.744.'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Licencia por excedencia',
                            'description' => 'Licencia por excedencia'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Licencia por maternidad Down',
                            'description' => 'Licencia por maternidad Down'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Licencia por vacaciones',
                            'description' => 'Licencia por vacaciones'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Licencia sin goce de haberes',
                            'description' => ' Licencia sin goce de haberes'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Reserva de puesto',
                            'description' => 'Reserva de puesto'));
                DB::table('sitrevision')
                ->insert(array('name' => 'E.S.E. Cese transitorio de servicios (Art. 6°, incisos 6 y 7 del Decreto N° 342/92)',
                            'description' => 'E.S.E. Cese transitorio de servicios (Art. 6°, incisos 6 y 7 del Decreto N° 342/92)'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Personal Siniestrazo de terceros',
                            'description' => 'Personal Siniestrazo de terceros'));
                DB::table('sitrevision')
                ->insert(array('name' => 'Reingreso por disposición judicial',
                            'description' => 'Reingreso por disposición judicial'));
                DB::table('sitrevision')
                ->insert(array('name' => 'ILT (Incapacidad Laboral Transitoria) primeros DIEZ (10) días',
                            'description' => ' ILT (Incapacidad Laboral Transitoria) primeros DIEZ (10) días'));
                DB::table('sitrevision')
                ->insert(array('name' => ' ILT (Incapacidad Laboral Transitoria) días ONCE (11) y siguientes',
                            'description' => 'ILT (Incapacidad Laboral Transitoria) días ONCE (11) y siguientes'));

                DB::table('sitrevision')
                ->insert(array('name' => ' Trabajador siniestrado en nómina de A.R.T.',
                            'description' => 'Trabajador siniestrado en nómina de A.R.T.'));

               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('sitrevision');
    }
}
