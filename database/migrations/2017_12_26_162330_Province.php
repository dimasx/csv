<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Province extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('province', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name');
            $table->string('description');
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);
            $table->timestamps();
        });

                DB::table('province')
                ->insert(array('name' => 'Buenos Aires',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Catamarca',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Ciudad Autónoma de Buenos Aires',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Chaco',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Chubut',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Córdoba',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Corrientes',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Entre Ríos',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Formosa',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Jujuy',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'La Pampa',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'La Rioja',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Mendoza',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Misiones',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Neuquén',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Río Negro',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Salta',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'San Juan',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'San Luis',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Santa Cruz',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Santa Fe',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Santiago del Estero',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Tierra del Fuego, Antártida e Islas del Atlántico Sur',
                            'description' => 'Argentina'));
                DB::table('province')
                ->insert(array('name' => 'Tucumán',
                            'description' => 'Argentina'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('province');
    }
}
