<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeConstant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('employeeconstants', function (Blueprint $table) {
            $table->increments('idn');
            $table->integer('idnperiod');
            $table->integer('idnsubperiod');
            $table->integer('idnconstant');
            $table->float('value');
            $table->string('comment');
            $table->integer('idnemployee');
            $table->integer('lock')->default(1); 
            $table->integer('active')->default(1);         
            $table->timestamps();
        });
            DB::table('employeeconstants')
                        ->insert(array(
                            'idnperiod' => 1,
                            'idnsubperiod' => 1,
                            'idnconstant' => 1,
                            'value' => 1,
                            'comment' => 'valor para vacaciones',
                            'idnemployee' => 1
                            ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('employeeconstants');
    }
}
