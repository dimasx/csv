<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeVacations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('employeevacations', function (Blueprint $table) {
            $table->increments('idn');
            $table->integer('idnperiod');
            $table->integer('idnsubperiod');
            $table->date('startdate');
            $table->date('finishdate');
            $table->integer('year');
            $table->integer('idnemployee');
            $table->integer('cant');
            $table->integer('lock')->default(1); 
            $table->integer('active')->default(1);         
            $table->timestamps();
        });
            DB::table('employeevacations')
                        ->insert(array(
                            'idnperiod' => 1,
                            'idnsubperiod' => 1,
                            'startdate' => '2017-12-12',
                            'finishdate' => '2018-08-12',
                            'year' => '2012',
                            'idnemployee' => 1,
                            'cant' => 3
                            ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employeevacations');
    }
}
