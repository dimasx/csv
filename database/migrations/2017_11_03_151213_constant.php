<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Constant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
     //Crear Tabla
            Schema::create('constant', function (Blueprint $table) {
            //Autoincrementable
            $table->increments('idn');
            //Datos especificos de tabla
            $table->string('cod');
            $table->string('name');
            $table->string('description');
           
         
            //Datos por default en todas las tablas
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);  
            $table->timestamps();
        });
               DB::table('constant')
                        ->insert(array( 'cod' => 'PRESENTVALOR',
                                        'name'=>'Presentismo Manual',
                                        'description'=>'Valor para Presentismo'
                                        
                                      
                                       ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('constant');
    }
}
