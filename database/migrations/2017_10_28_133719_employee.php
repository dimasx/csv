<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Employee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) 
        {
            $table->increments('idn');
            $table->string('cod');
            $table->string('firstname');
            $table->string('lastname');
            $table->integer('gender');
            $table->string('dni')->unique();
            $table->string('cuil')->unique();
            $table->date('birthdate');
            $table->string('photo');
            $table->string('city');
            $table->string('province');
            $table->integer('postalcode');
            $table->string('street');
            $table->string('number');
            $table->string('floor');
            $table->string('department');
            $table->string('telephone');
            $table->string('cellphone');
            $table->string('workemail');
            $table->string('personemail');

            $table->integer('idnnationality');
            $table->integer('idncivilstatus');
            $table->integer('idnlevelstudies');
            $table->integer('idncostcenter');
            $table->integer('idndepartment');
            $table->integer('idnpositionjob');
            $table->integer('idnworkcalendar');
            $table->integer('idncct');
            $table->integer('idncategorysalary');
            $table->integer('idnexitreason');
            $table->integer('idnsocialwork');
            $table->integer('idncompany');       
            $table->integer('idnsitrevision');
            $table->integer('idnmodcontract');
            $table->integer('idnworkplace');
            $table->integer('idnroster');
            $table->integer('idnsindicate');
            $table->integer('idnregprevisional');
            $table->integer('idnsinestercode');
            $table->integer('idnemployeecondition');
            $table->integer('idnmedicalplan');

            $table->float('basicsalary');            
            $table->integer('employeestatus');
            
            $table->date('dateentry');
            $table->date('dateentryrecognized');
            $table->date('dateexit');  
            $table->integer('antiquity');
            $table->integer('adherents');           
            $table->integer('numberofcap');            
            $table->integer('affiliate');            
            $table->integer('lifesecure');
            $table->integer('benefitsoccharges');
            $table->integer('agreed');
            
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);
            $table->timestamps();
        });

        DB::table('employee')
                ->insert(array('cod' => '1016',
                               'firstname' => 'pedro',
                               'lastname'=> 'perez',
                               'gender'=>1, 
                               'dni'=>'5555',
                               'cuil'=>'5555',
                               'birthdate'=>'1994/05/05',
                               'photo'=>'IMAGEN1.PNP',
                               'city'=>'CABA',
                               'province'=>'Buenos aires',
                               'postalcode'=>0063,
                               'street'=>'calle 23a',
                               'number'=>'23',
                               'floor'=>'Tercero',
                               'department'=>'C24',
                               'telephone'=>'0416-45454',
                               'cellphone'=>'0416-4524',
                               'workemail'=>'pedro@emplearte.com',
                               'personemail'=>'pedro@gmail.com',

                               'idnnationality'=>1,
                               'idncivilstatus'=>1,
                               'idnlevelstudies'=>1,
                               'idncostcenter'=>1,
                               'idndepartment'=>1,
                               'idnpositionjob'=>1,
                               'idnworkcalendar'=>1,                               
                               'idncct'=>1,
                               'idncategorysalary'=>1,
                               'idnexitreason'=> 1,
                               'idnsocialwork'=> 1,
                               'idncompany'=> 1,
                               'idnsitrevision'=> 1,
                               'idnmodcontract'=> 1,
                               'idnworkplace'=> 1,
                               'idnroster'=> 1,
                               'idnsindicate'=> 1,
                               'idnregprevisional'=> 1,
                               'idnsinestercode' => 1,
                               'idnemployeecondition'=>1,
                               'idnmedicalplan'=>1,
                               
                               
                               'basicsalary'=>133,
                               'employeestatus'=>1,
                               'dateentry'=>'1994/05/05',
                               'dateentryrecognized'=>'1994/05/05',
                               'dateexit'=>'1994/05/05',
                               'antiquity'=> 1,
                               'adherents'=> 1,
                               'numberofcap'=> 1,
                               'affiliate'=> 1,  
                               'lifesecure'=> 1,
                               'benefitsoccharges'=> 1,
                               'agreed'=> 1
                               ));

                           // $table->increments('idn');
            

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
