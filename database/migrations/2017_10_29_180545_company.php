
    <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Company extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
        Schema::create('company', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('cod');
            $table->string('cuit');
            $table->string('cuiladmin');
            $table->integer('idnfather');
            $table->integer('idncompanystatus')->default(1); 
            $table->integer('idncompanytype');   //falta la tabla
            $table->integer('idncompanyactivity');   //falta la tabla
            $table->integer('idndatasource')->default(1);
            $table->integer('idntypeperson');
            $table->integer('idncompanyzone');
            $table->string('photologo');
            $table->string('companyactivity');
            $table->string('companycct');
            $table->string('fiscaldomicile');
            $table->string('photosing');
            $table->integer('benefitsoccharges');
            $table->integer('monthclose');
            $table->string('observation');
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);
            $table->timestamps();
        });

        DB::table('company')
                ->insert(array(

                  'name' => 'polar',
                  'cod' => 'centralcaribe',
                  'cuit' => '123234234',
                  'cuiladmin' => '12314232',
                  'idnfather' => 1,
                  'idncompanystatus' => 1,
                  'idncompanytype' => 1,
                  'idncompanyactivity' => 1,
                  'idndatasource' => 1,
                  'idntypeperson' => 1,
                  'idncompanyzone' => 1,
                  'photologo' => 'logo.png',
                  'companyactivity' => 'Comercio',
                  'companycct' => 'CCT Comercio',
                  'fiscaldomicile' => 'CABA',
                  'photosing' => 'firma.png',
                  'benefitsoccharges' => 1,
                  'monthclose' => 12,
                  'observation' => 'Ninguna',
                            
                  ));

               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}


