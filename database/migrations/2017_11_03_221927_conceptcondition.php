<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Conceptcondition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('conceptcondition', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

       DB::table('conceptcondition')
                ->insert(array('name' => 'Por Defecto',
                                'description' => 'Por Defecto'
                                ));
                               
                DB::table('conceptcondition')
                ->insert(array('name' => 'Ausencia Justificada',
                                'description' => 'entrego comprobante'
                                ));

                 DB::table('conceptcondition')
                ->insert(array('name' => 'Ausencia Injustificada',
                                'description' => 'No entrego comprobante'
                                ));
                 DB::table('conceptcondition')
                ->insert(array('name' => 'Termiancion',
                                'description' => 'Normal'
                                ));
                 DB::table('conceptcondition')
                ->insert(array('name' => 'SAC',
                                'description' => 'Normal'
                                ));
        
                  DB::table('conceptcondition')
                ->insert(array('name' => 'Feriado','description' => 'Normal'));
               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
           Schema::dropIfExists('conceptcondition');
    }
}
