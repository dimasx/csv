<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Concept extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     //Crear Tabla
            Schema::create('concept', function (Blueprint $table) {
            //Autoincrementable
            $table->increments('idn');
            //Datos especificos de tabla
            $table->string('cod');
            $table->string('name');
            $table->integer('idncolumn');
            $table->integer('idncondition');
         
            //Datos por default en todas las tablas
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);  
            $table->timestamps();
        });
        
        DB::table('concept')
                        ->insert(array('cod' => 'sbasi','name'=>'Sueldo Básico','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'futau','name'=>'A Cta. Futuros Aumentos','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'grext','name'=>'Gratif. Extra. No Remunerativa','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'dnotr','name'=>'Dto. días no Trabajados','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'dgrem','name'=>'Dia del Gremio','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'hnorm','name'=>'Horas Normales','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'hnoct','name'=>'Horas Nocturnas','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'hferi','name'=>'Horas Feriado','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'henfe','name'=>'Horas Enfermedad','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'hrart','name'=>'Horas ART','idncolumn'=>1,'idncondition'=>1));
        
        DB::table('concept')
                        ->insert(array('cod' => 'hlact','name'=>'Horas Lactancia','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'hligr','name'=>'Horas Lic. Gremial','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'hexam','name'=>'Horas Examen','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'hm200','name'=>'   Más de 200 Horas','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'hsfer','name'=>'   Hs Feriado (Norm+Noct)','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'hnadi','name'=>'   Horas Noct + Adic 25%','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'hex10','name'=>'   Horas Extras 100%','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'hmuda','name'=>   'Horas Mudanza','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'adfer','name'=>'   Adicional Feriado','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'hefn','name'=>'    Horas Enfermedad Noct','idncolumn'=>1,'idncondition'=>1));
        
        DB::table('concept')
                        ->insert(array('cod' => 'cfaum','name'=>'   A Cta. Futuros Aumentos','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'advol','name'=>'   Adicional Voluntario','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'hexn5','name'=>'   Horas Extras Nocturnas 50%','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'ahenf','name'=>'   Aj. Horas Enfermedad','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'ahn25','name'=>'   Aj. Horas Nocturnas + 25%','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'hreem','name'=>'	Horas Reemplazo','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'ft300','name'=>'   Feriado Trabajado 300%','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'hefac','name'=>'   Horas Enfermedad Familiar a cargo','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'hlmat','name'=>'   Horas Lic Matrimonio','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'hlffa','name'=>'	Horas Lic Fallecimiento Familiar','idncolumn'=>1,'idncondition'=>1));
         
        DB::table('concept')
                        ->insert(array('cod' => 'hlnac','name'=>'	Horas Lic Nacimiento','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'bon10','name'=>'	Bonificación 10%','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'p8mhn','name'=>'   Pago 8 Min x Hs Noct ','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'hex50','name'=>'	Hs Extras 50%','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'aturn','name'=>'	Adicional turno','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'ppqmi','name'=>'	Premio Pres Quimico','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'prmac','name'=>'	Premio Actitud  ','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'ppnoc','name'=>'	Plus Premio Noct ','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'sfsol','name'=>'	Suma Fija Solidaria','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'adcal','name'=>'	Adicional Calidad','idncolumn'=>1,'idncondition'=>1));
        
        DB::table('concept')
                        ->insert(array('cod' => 'adenc','name'=>'	Adicional Encargado','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'ajfer','name'=>'	Aj. Feriado','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'ajatu','name'=>'	Aj. Adicional Turno','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'ajase','name'=>'	Aj. Adicional Sector','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'ajati','name'=>'	Aj. Adic. Título','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'renot','name'=>'	Recargo Nocturno','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'hen10','name'=>'	Horas Extras Nocturnas 100%','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'mafon','name'=>'   Manejo de Fondo','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'enprv','name'=>'	Enfermedad PRV','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'artpr','name'=>'	ART PRV','idncolumn'=>1,'idncondition'=>1));
        
        DB::table('concept')
                        ->insert(array('cod' => 'bxant','name'=>'	Bonif. por antigüedad','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'ajboa','name'=>'	Aj. Bonificación Antiguedad','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'pestm','name'=>'	Presentismo','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'acfau','name'=>'	A cuenta futuros aumentos','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'sprop','name'=>'	SAC proporcional','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'lxenf','name'=>'	Lic. por enfermedad','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'pxmatr','name'=>'	Permiso por matrimonio','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'pxnah','name'=>'	Permiso por nac. de hijo','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'sac00','name'=>'	SAC','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'lsgds','name'=>'	Lic. sin goce de sueldo','idncolumn'=>1,'idncondition'=>1));
        
        DB::table('concept')
                        ->insert(array('cod' => 'ladtr','name'=>'	Lic. por accidente de trabajo','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'lxmat','name'=>'	Lic. por maternidad','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'lpexc','name'=>'   Lic. por excedencia','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'lpexa','name'=>'	Lic. por Exámen','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'lffam','name'=>'	Lic. Fallecimiento Familiar','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'dhdha','name'=>'	Disp. horaria días hábiles','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'lmatr','name'=>'	Lic. Matrimonio','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'lnhij','name'=>'	Lic. Nacimiento Hijo','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'feria','name'=>'	Feriado','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'vgoza','name'=>'   Vacaciones gozadas','idncolumn'=>1,'idncondition'=>1));
        
        DB::table('concept')
                        ->insert(array('cod' => 'plvac','name'=>'	Plus Vacacional','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'vgozn','name'=>'	Vacaciones Gozadas (Nocturnas)','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'paspe','name'=>'	Premio Asistencia Perfecta','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'aptit','name'=>'	Adic. por título','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'apima','name'=>'	Adic. por idioma','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'tarpe','name'=>'   Tareas peligrosas','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'cserv','name'=>'	Complemento Servicio','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'pluss','name'=>'   Plus','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'adino','name'=>'   Adicional Nocturno','idncolumn'=>1,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'refri','name'=>'   Refrigerio','idncolumn'=>1,'idncondition'=>1));
       
        DB::table('concept')
                        ->insert(array('cod' => 'hex50','name'=>'	Horas extras 50%','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'viati','name'=>'   Viáticos','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'arnbo','name'=>'	Asig. remun. no bonif.','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'dhfin','name'=>'	Disp. horaria feriados e Ihna.','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'btple','name'=>'	Bonif. tiempo pleno','idncolumn'=>1,'idncondition'=>1));
        DB::table('concept')
                        ->insert(array('cod' => 'bpfun','name'=>'	Bonif. por función','idncolumn'=>1,'idncondition'=>1));
       
                                 
        DB::table('concept')
                        ->insert(array('cod' => 'znfri','name'=>'	Zona Fría','idncolumn'=>2,'idncondition'=>1));
         DB::table('concept')
                        ->insert(array('cod' => 'antba','name'=>'	Antiguedad Base','idncolumn'=>1,'idncondition'=>1));
        
                                 
        DB::table('concept')     
                        ->insert(array('cod' => 'ley19','name'=>'	Ley 19.032','idncolumn'=>3,'idncondition'=>1));
                                 
        DB::table('concept')     
                        ->insert(array('cod' => 'obrso','name'=>'	Obra social','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'csind','name'=>'	Cuota sindical','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'faecy','name'=>'	FAECYS','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'asoli','name'=>'	Aporte solidario','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'dtova','name'=>'	Dto. vacaciones','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'ausin','name'=>'	Ausencia injustificada','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'segvi','name'=>'	Seguro de vida','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'ansue','name'=>'	Anticipo de sueldo','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'dtoas','name'=>'	Dto. Anticipo de sueldo','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'svobl','name'=>'	Seguro de vida obligatorio','idncolumn'=>3,'idncondition'=>1));
        
        DB::table('concept')     
                        ->insert(array('cod' => 'cssti','name'=>'	Cuota Sindical STIPA','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'cgrmi','name'=>'	Cuota Gremial','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'seccc','name'=>'	SEC (Art 100 CCT)','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'sdvys','name'=>'	Seguro de Vida y Sepelio','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'falca','name'=>'	Falla de Caja','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'afaso','name'=>'	Aporte Fondo Asistencia Social','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'aaoss','name'=>'	Aporte Adicional O.S.','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'dlpex','name'=>'	Dto. Lic. por excedencia','idncolumn'=>3,'idncondition'=>1));
        DB::table('concept')     
                        ->insert(array('cod' => 'dlpma','name'=>'	Dto. Lic. por maternidad','idncolumn'=>3,'idncondition'=>1));                
        DB::table('concept')     
                        ->insert(array('cod' => 'dlsgs','name'=>'	Dto. Lic. sin goce de sueldo','idncolumn'=>3,'idncondition'=>1));
        
    DB::table('concept')     
                        ->insert(array('cod' => 'vctca','name'=>'	Viáticos CCT Camioneros','idncolumn'=>3,'idncondition'=>1));
    DB::table('concept')     
                        ->insert(array('cod' => 'cctca','name'=>'	Comida CCT Camioneros','idncolumn'=>3,'idncondition'=>1));
    DB::table('concept')     
                        ->insert(array('cod' => 'embar','name'=>'	Embargo','idncolumn'=>3,'idncondition'=>1));
    DB::table('concept')     
                        ->insert(array('cod' => 'subfa','name'=>'	Subsidio Por Fallecimiento','idncolumn'=>3,'idncondition'=>1));
    DB::table('concept')     
                        ->insert(array('cod' => 'rytur','name'=>'	Recreación y Turismo','idncolumn'=>3,'idncondition'=>1));
    DB::table('concept')     
                        ->insert(array('cod' => 'apmut','name'=>'	Aporte Mutual','idncolumn'=>3,'idncondition'=>1));
    DB::table('concept')     
                        ->insert(array('cod' => 'acyas','name'=>'	Aporte Capacitación y Acción Social','idncolumn'=>3,'idncondition'=>1));
    DB::table('concept')     
                        ->insert(array('cod' => 'ajubi','name'=>'	Aporte jubilatorio','idncolumn'=>3,'idncondition'=>1));
    DB::table('concept')     
                        ->insert(array('cod' => 'sdvco','name'=>'	Seg. de vida col. oblig. cnas','idncolumn'=>3,'idncondition'=>1));
    DB::table('concept')     
                        ->insert(array('cod' => 'redon','name'=>    'Redondeo','idncolumn'=>3,'idncondition'=>1));
    DB::table('concept')     
                        ->insert(array('cod' => 'aioma','name'=>'	Aporte IOMA','idncolumn'=>3,'idncondition'=>1));
    DB::table('concept')     
                        ->insert(array('cod' => 'dafip','name'=>'	Descuento A.F.I.P.','idncolumn'=>3,'idncondition'=>1));
    DB::table('concept')     
                        ->insert(array('cod' => 'rialg','name'=>'   Ret. Impuesto a las Ganancias','idncolumn'=>3,'idncondition'=>1));
   /* DB::table('concept')     
                        ->insert(array('cod' => '25001','name'=>'	Suma no remunerativa','idncolumn'=>4,'idncondition'=>1)
                                  DB::table('concept')     
                        ->insert(array('cod' => '25001','name'=>'	Presentismo s/ No remun.','idncolumn'=>4,'idncondition'=>1)
                                  DB::table('concept')     
                        ->insert(array('cod' => '25001','name'=>'Bonif. Antiguedad s/No Remunerativo','idncolumn'=>4,'idncondition'=>1)
                                  DB::table('concept')     
                        ->insert(array('cod' => '25001','name'=>'	Suma no remunerativa s/presentismo','idncolumn'=>4,'idncondition'=>1)
                                  DB::table('concept')     
                        ->insert(array('cod' => '25001','name'=>'	Indemnizacion por antigüedad','idncolumn'=>4,'idncondition'=>1)
                                  DB::table('concept')     
                        ->insert(array('cod' => '25001','name'=>'Integr. mes despido','idncolumn'=>4,'idncondition'=>1)
                                  DB::table('concept')     
                        ->insert(array('cod' => '25001','name'=>'	SAC s/integr. mes despido','idncolumn'=>4,'idncondition'=>1)
                                  DB::table('concept')     
                        ->insert(array('cod' => '25001','name'=>'	Indemn. sust. preaviso','idncolumn'=>4,'idncondition'=>1)
                                  DB::table('concept')     
                        ->insert(array('cod' => '25001','name'=>'	SAC s/indemn. sust. preaviso','idncolumn'=>4,'idncondition'=>1)
                                  DB::table('concept')     
                        ->insert(array('cod' => '25001','name'=>'Vac. no gozadas','idncolumn'=>4,'idncondition'=>1)
                                  DB::table('concept')     
                        ->insert(array('cod' => '25001','name'=>'	SAC s/vac. no gozadas','idncolumn'=>4,'idncondition'=>1)*/
   
                               
                      
                        
                                      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('concept');
    }
}
