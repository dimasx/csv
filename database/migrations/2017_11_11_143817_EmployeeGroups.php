<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeGroups extends Migration
{
       /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
            //Crear Tabla
            Schema::create('employeegroups', function (Blueprint $table) {
            //Autoincrementable
            $table->increments('idn');
            $table->integer('idngroup');
            $table->integer('idnemployee');
            //Datos por default en todas las tablas
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);  
            $table->timestamps();
        });
               DB::table('employeegroups')
                        ->insert(array( 
                            'idngroup' => 1,
                            'idnemployee' => 1
                                                     
                                       ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('employeegroups');
    }
}
