<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sinestercode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('sinestercode', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

         DB::table('sinestercode')
                ->insert(array('name' => 'No Incapacitado',
                            'description' => 'No Incapacitado'));
                DB::table('sinestercode')
                ->insert(array('name' => 'ILT Incapacidad Laboral Temporaria',
                            'description' => 'ILT Incapacidad Laboral Temporaria'));
                DB::table('sinestercode')
                ->insert(array('name' => 'ILPPP Incapacidad Laboral Permanente Parcial Provisoria.',
                            'description' => 'ILPPP Incapacidad Laboral Permanente Parcial Provisoria.'));
                DB::table('sinestercode')
                ->insert(array('name' => 'ILPPD Incapacidad Laboral Permanente Parcial Definitiva.',
                            'description' => ' ILPPD Incapacidad Laboral Permanente Parcial Definitiva.'));
                DB::table('sinestercode')
                ->insert(array('name' => 'ILPTP Incapacidad Laboral Permanente Total Provisoria',
                            'description' => 'ILPTP Incapacidad Laboral Permanente Total Provisoria'));
                DB::table('sinestercode')
                ->insert(array('name' => 'Capital de recomposición Art. 15, ap. 3, Ley Nº 24.557',
                            'description' => 'Capital de recomposición Art. 15, ap. 3, Ley Nº 24.557'));
                DB::table('sinestercode')
                ->insert(array('name' => 'Ajuste Definitivo ILPPD de pago mensual',
                            'description' => 'Ajuste Definitivo ILPPD de pago mensual'));
                DB::table('sinestercode')
                ->insert(array('name' => 'RENTA PERIODICA ILPPD Inc Lab Perm Parc Def >50%<66%',
                            'description' => 'RENTA PERIODICA ILPPD Inc Lab Perm Parc Def >50%<66%SRT/SSN F.Garantía/F Reserva ILT Incapacidad Laboral Temporaria'));
                DB::table('sinestercode')
                ->insert(array('name' => 'SRT/SSN F.Garantía/F Reserva ILT Incapacidad Laboral Temporaria',
                            'description' => 'SRT/SSN F.Garantía/F Reserva ILT Incapacidad Laboral Temporaria'));
                DB::table('sinestercode')
                ->insert(array('name' => 'SRT/SSN F.Garantía/F Reserva ILPPP Inc Lab Perm Parc Prov',
                            'description' => 'SRT/SSN F.Garantía/F Reserva ILPPP Inc Lab Perm Parc Prov'));
                DB::table('sinestercode')
                ->insert(array('name' => 'SRT/SSN F.Garantía/F Reserva ILPTP Inc Lab Perm Total Prov',
                            'description' => 'SRT/SSN F.Garantía/F Reserva ILPTP Inc Lab Perm Total Prov'));
                DB::table('sinestercode')
                ->insert(array('name' => 'SRT/SSN F.Garantía/F Reserva ILPPD Inc Laboral Perm Parc Definitiva',
                            'description' => 'SRT/SSN F.Garantía/F Reserva ILPPD Inc Laboral Perm Parc Definitiva'));
                DB::table('sinestercode')
                ->insert(array('name' => 'ILPPD Beneficios devengados art. 11 p.4',
                            'description' => 'ILPPD Beneficios devengados art. 11 p.4'));
                DB::table('sinestercode')
                ->insert(array('name' => 'INFORME Incremento salarial de trabajador siniestrado a ART',
                            'description' => 'INFORME Incremento salarial de trabajador siniestrado a ART'));


               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sinestercode');
    }
}
