<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Socworks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socworks', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

        DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA GRAFICA DE LA PROVINCIA DE CORDOBA',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA GRAFICA DE LA PROVINCIA DE CORDOBA'));
             DB::table('socworks')
                ->insert(array('name' => 'O. S. PORTUARIOS ARGENTINOS DE MAR DEL PLATA',
                            'description' => 'O. S. PORTUARIOS ARGENTINOS DE MAR DEL PLATA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DEL ORGANISMO DE CONTROL EXTERNO',
                            'description' => 'O. S. DEL PERSONAL DEL ORGANISMO DE CONTROL EXTERNO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE CAPITANES, PILOTOS Y PATRONES DE PESCA',
                            'description' => 'O. S. DE CAPITANES, PILOTOS Y PATRONES DE PESCA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE AGENTES DE LOTERIAS Y AFINES DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DE AGENTES DE LOTERIAS Y AFINES DE LA REPUBLICA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'MUTUAL DEL PERSONAL DEL AGUA Y LA ENERGIA ELECTRICA DE MENDOZA',
                            'description' => 'MUTUAL DEL PERSONAL DEL AGUA Y LA ENERGIA ELECTRICA DE MENDOZA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE Y.P.F.',
                            'description' => 'O. S. DE Y.P.F.l'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LA ACTIVIDAD DE SEGUROS, REASEGUROS, CAPITALIZACION Y AHORRO Y PRESTAMO PARA LA VIVIENDA',
                            'description' => 'O. S. DE LA ACTIVIDAD DE SEGUROS, REASEGUROS, CAPITALIZACION Y AHORRO Y PRESTAMO PARA LA VIVIENDA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PARA LA ACTIVIDAD DOCENTE',
                            'description' => 'O. S. PARA LA ACTIVIDAD DOCENTE'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PARA EL PERSONAL DEL MINISTERIO DE ECONOMIA Y DE OBRAS Y SERVICIOS PUBLICOS',
                            'description' => 'O. S. PARA EL PERSONAL DEL MINISTERIO DE ECONOMIA Y DE OBRAS Y SERVICIOS PUBLICOS'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. FERROVIARIA',
                            'description' => ' O. S. FERROVIARIA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LA ASOCIACION CIVIL PRO SINDICATO AMAS DE CASA DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DE LA ASOCIACION CIVIL PRO SINDICATO AMAS DE CASA DE LA REPUBLICA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL SINDICATO UNIDO DE TRABAJADORES DE LA INDUSTRIA DE AGUAS GASEOSAS DE LA PROVINCIA DE SANTA FE',
                            'description' => 'O. S. DEL SINDICATO UNIDO DE TRABAJADORES DE LA INDUSTRIA DE AGUAS GASEOSAS DE LA PROVINCIA DE SANTA FE'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL JERARQUICO DE LA REPUBLICA ARGENTINA PARA EL PERSONAL JERARQUICO DE LA INDUSTRIA GRAFICA Y EL PERSONAL JERARQUICO DEL AGUA Y LA ENERGIA',
                            'description' => 'O. S. DEL PERSONAL JERARQUICO DE LA REPUBLICA ARGENTINA PARA EL PERSONAL JERARQUICO DE LA INDUSTRIA GRAFICA Y EL PERSONAL JERARQUICO DEL AGUA Y LA ENERGIA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LOS LEGISLADORES DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DE LOS LEGISLADORES DE LA REPUBLICA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LA FEDERACION DE CAMARAS Y CENTROS COMERCIALES ZONALES DE LA REPUBLICA ARGENTINA (O. S. FEDECAMARAS)',
                            'description' => 'O. S. DE LA FEDERACION DE CAMARAS Y CENTROS COMERCIALES ZONALES DE LA REPUBLICA ARGENTINA (O. S. FEDECAMARAS)'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. de VIALIDAD NACIONAL',
                            'description' => 'O. S. de VIALIDAD NACIONAL'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PROFESIONALES DEL TURF DE LA RA (OSPROTURA)',
                            'description' => 'O. S. PROFESIONALES DEL TURF DE LA RA (OSPROTURA)'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE EMPL. Y PERS. JERARQ. DE LA ACT. DEL NEUM. ARG. DE NEUM. G. YEAR',
                            'description' => 'O. S. DE EMPL. Y PERS. JERARQ. DE LA ACT. DEL NEUM. ARG. DE NEUM. G. YEAR'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. PARA EL PERSONAL DE EMPRESAS DE LIMPIEZA, SERVICIOS Y MAESTRANZA DE MENDOZA',
                            'description' => 'O. S. PARA EL PERSONAL DE EMPRESAS DE LIMPIEZA, SERVICIOS Y MAESTRANZA DE MENDOZA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL JERARQUICO DEL TRANSPORTE AUTOMOTOR DE PASAJEROS DE CORDOBA Y AFINES',
                            'description' => 'O. S. DEL PERSONAL JERARQUICO DEL TRANSPORTE AUTOMOTOR DE PASAJEROS DE CORDOBA Y AFINES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE MINISTROS, SECRETARIOS Y SUBSECRETARIOS',
                            'description' => 'O. S. DE MINISTROS, SECRETARIOS Y SUBSECRETARIOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LOS TRABAJADORES DE LA CARNE Y AFINES DE LA REPUBLICA ARGENTINA (OSCARA)',
                            'description' => 'O. S. DE LOS TRABAJADORES DE LA CARNE Y AFINES DE LA REPUBLICA ARGENTINA (OSCARA)'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. ARA EL PERSONAL DE LA INDUSTRIA ACEITERA DESMOTADORA Y AFINES',
                            'description' => 'O. S. ARA EL PERSONAL DE LA INDUSTRIA ACEITERA DESMOTADORA Y AFINES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE ACTORES',
                            'description' => 'O. S. DE ACTORES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE TECNICOS DE VUELO DE LINEAS AEREAS',
                            'description' => 'O. S. DE TECNICOS DE VUELO DE LINEAS AEREAS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL SUPERIOR Y PROFESIONAL DE EMPRESAS AEROCOMERCIALES',
                            'description' => 'O. S. DEL PERSONAL SUPERIOR Y PROFESIONAL DE EMPRESAS AEROCOMERCIALES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL AERONAUTICO',
                            'description' => 'O. S. DEL PERSONAL AERONAUTICO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE AERONAVEGACION DE ENTES PRIVADOS',
                            'description' => ' O. S. DEL PERSONAL DE AERONAVEGACION DE ENTES PRIVADOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL TECNICO AERONAUTICO',
                            'description' => 'O. S. DEL PERSONAL TECNICO AERONAUTICO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE AERONAVEGANTES',
                            'description' => 'O. S. DE AERONAVEGANTES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE EMPLEADOS DE AGENCIAS DE INFORMES (O.S.E.D.I.)',
                            'description' => 'O. S. DE EMPLEADOS DE AGENCIAS DE INFORMES (O.S.E.D.I.)'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE AGUAS GASEOSAS Y AFINES',
                            'description' => 'O. S. DEL PERSONAL DE AGUAS GASEOSAS Y AFINES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE ALFAJOREROS, REPOSTEROS, PIZZEROS Y HELADEROS',
                            'description' => 'O. S. DE ALFAJOREROS, REPOSTEROS, PIZZEROS Y HELADEROS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DE LA ALIMENTACION',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DE LA ALIMENTACION'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE ARTES GRAFICAS DEL CHACO',
                            'description' => 'O. S. DEL PERSONAL DE ARTES GRAFICAS DEL CHACO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. ARTES GRAFICAS DE SANTA FE',
                            'description' => 'O. S. ARTES GRAFICAS DE SANTA FE'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE ARTISTAS DE VARIEDADES',
                            'description' => 'O. S. DE ARTISTAS DE VARIEDADES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DEL AUTOMOVIL CLUB ARGENTINO',
                            'description' => 'O. S. DEL PERSONAL DEL AUTOMOVIL CLUB ARGENTINO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DEL AZUCAR DE CALILEGUA',
                            'description' => 'O. S. DEL PERSONAL DEL AZUCAR DE CALILEGUA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DEL AZUCAR DEL INGENIO LA ESPERANZA',
                            'description' => 'O. S. DEL PERSONAL DEL AZUCAR DEL INGENIO LA ESPERANZA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DEL AZUCAR DEL INGENIO LEDESMA',
                            'description' => 'O. S. DEL PERSONAL DEL AZUCAR DEL INGENIO LEDESMA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DEL AZUCAR DEL INGENIO LAS TOSCAS',
                            'description' => 'O. S. DEL PERSONAL DEL AZUCAR DEL INGENIO LAS TOSCAS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DEL AZUCAR DEL INGENIO RIO GRANDE',
                            'description' => 'O. S. DEL PERSONAL DEL AZUCAR DEL INGENIO RIO GRANDE'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DEL AZUCAR DEL INGENIO SAN ISIDRO',
                            'description' => 'O. S. DEL PERSONAL DEL AZUCAR DEL INGENIO SAN ISIDRO'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DEL AZUCAR DEL INGENIO SAN MARTIN',
                            'description' => ' O. S. DEL PERSONAL DEL AZUCAR DEL INGENIO SAN MARTIN'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DEL AZUCAR DE VILLA OCAMPO',
                            'description' => 'O. S. DEL PERSONAL DEL AZUCAR DE VILLA OCAMPO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD AZUCARERA TUCUMANA',
                            'description' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD AZUCARERA TUCUMANA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA AZUCARERA',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA AZUCARERA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE BANCARIOS',
                            'description' => 'O. S. DE BANCARIOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSON. DE BARRACAS DE LANAS, CUEROS Y ANEXOS',
                            'description' => 'O. S. DEL PERSON. DE BARRACAS DE LANAS, CUEROS Y ANEXOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA BOTONERA',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA BOTONERA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL CALZADO',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL CALZADO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE CONDUCTORES CAMIONEROS Y PERSONAL DEL TRANSPORTE AUTOMOTOR DE CARGAS',
                            'description' => 'O. S. DE CONDUCTORES CAMIONEROS Y PERSONAL DEL TRANSPORTE AUTOMOTOR DE CARGAS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA JUNTA NACIONAL DE CARNES',
                            'description' => 'O. S. DEL PERSONAL DE LA JUNTA NACIONAL DE CARNES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE CARGA Y DESCARGA',
                            'description' => 'O. S. DEL PERSONAL DE CARGA Y DESCARGA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL AUXILIAR DE CASAS PARTICULARES',
                            'description' => 'O. S. DEL PERSONAL AUXILIAR DE CASAS PARTICULARES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL CAUCHO',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL CAUCHO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DEL CAUCHO',
                            'description' => 'O. S. DEL PERSONAL DEL CAUCHO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL CAUCHO DE SANTA FE',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL CAUCHO DE SANTA FE'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE CEMENTERIOS',
                            'description' => 'O. S. DEL PERSONAL DE CEMENTERIOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE CERAMISTAS',
                            'description' => 'O. S. DE CERAMISTAS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA CERAMICA, SANITARIOS, PORCELANA DE MESA Y AFINES',
                            'description' => 'O. S. DEL PERSONAL DE LA CERAMICA, SANITARIOS, PORCELANA DE MESA Y AFINES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD CERVECERA Y AFINES',
                            'description' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD CERVECERA Y AFINES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL CINEMATOGRAFICO DE MAR DEL PLATA',
                            'description' => 'O. S. DEL PERSONAL CINEMATOGRAFICO DE MAR DEL PLATA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA CINEMATOGRAFICA',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA CINEMATOGRAFICA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE OPERADORES CINEMATOGRAFICOS',
                            'description' => 'O. S. DE OPERADORES CINEMATOGRAFICOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE COLCHONEROS',
                            'description' => 'O. S. DE COLCHONEROS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE COLOCADORES DE AZULEJOS, MOSAICOS, GRANITEROS, LUSTRADORES Y PORCELANEROS',
                            'description' => 'O. S. DE COLOCADORES DE AZULEJOS, MOSAICOS, GRANITEROS, LUSTRADORES Y PORCELANEROS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE EMPLEADOS DE COMERCIO',
                            'description' => 'O. S. DE EMPLEADOS DE COMERCIO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE CONDUCTORES NAVALES',
                            'description' => 'O. S. DE CONDUCTORES NAVALES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE CONSIGNATARIOS DEL MERCADO GENERAL DE HACIENDA DE AVELLANEDA',
                            'description' => 'O. S. DE CONSIGNATARIOS DEL MERCADO GENERAL DE HACIENDA DE AVELLANEDA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL ADMINISTRATIVO Y TECNICO DE LA CONSTRUCCION Y AFINES',
                            'description' => 'O. S. DEL PERSONAL ADMINISTRATIVO Y TECNICO DE LA CONSTRUCCION Y AFINES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA CONSTRUCCION',
                            'description' => 'O. S. DEL PERSONAL DE LA CONSTRUCCION'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LOS CORTADORES DE LA INDUMENTARIA',
                            'description' => 'O. S. DE LOS CORTADORES DE LA INDUMENTARIA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL CUERO Y AFINES',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL CUERO Y AFINES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL CHACINADO Y AFINES',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL CHACINADO Y AFINES'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DE CHOFERES DE CAMIONES',
                            'description' => ' O. S. DE CHOFERES DE CAMIONES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. CHOFERES PARTICULARES',
                            'description' => 'O. S. CHOFERES PARTICULARES'));
                DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE ENTIDADES DEPORTIVAS Y CIVILES',
                            'description' => 'O. S. DEL PERSONAL DE ENTIDADES DEPORTIVAS Y CIVILES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE EMPLEADOS DE DESPACHANTES DE ADUANA',
                            'description' => 'O. S. DE EMPLEADOS DE DESPACHANTES DE ADUANA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERS. DE DISTRIBUIDORES CINEMATOGRAF. DE LA R.A.',
                            'description' => 'O. S. DEL PERS. DE DISTRIBUIDORES CINEMATOGRAF. DE LA R.A.'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE DOCENTES PARTICULARES
',
                            'description' => 'O. S. DE DOCENTES PARTICULARES
'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE EDIFICIOS DE RENTA Y HORIZONTAL DE LA REPUBLICA ARGENTINA',
                            'description' => ' O. S. DEL PERSONAL DE EDIFICIOS DE RENTA Y HORIZONTAL DE LA REPUBLICA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE EDIFICIOS DE RENTAS Y HORIZONTAL DE CAPITAL FEDERAL Y GRAN BUENOS ARIES',
                            'description' => 'O. S. DEL PERSONAL DE EDIFICIOS DE RENTAS Y HORIZONTAL DE CAPITAL FEDERAL Y GRAN BUENOS ARIES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. ELECTRICISTAS NAVALES',
                            'description' => 'O. S. ELECTRICISTAS NAVALES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE OBREROS EMPACADORES DE FRUTA DE RIO NEGRO Y NEUQUEN',
                            'description' => 'O. S. DE OBREROS EMPACADORES DE FRUTA DE RIO NEGRO Y NEUQUEN'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA ENSEÑANZA PRIVADA',
                            'description' => 'O. S. DEL PERSONAL DE LA ENSEÑANZA PRIVADA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE ESCRIBANIAS DE LA PROVINCIA DE BUENOS AIRES',
                            'description' => 'O. S. DEL PERSONAL DE ESCRIBANIAS DE LA PROVINCIA DE BUENOS AIRES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE ESCRIBANOS',
                            'description' => 'O. S. DEL PERSONAL DE ESCRIBANOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DEL ESPECTACULO PUBLICO',
                            'description' => 'O. S. DEL PERSONAL DEL ESPECTACULO PUBLICO'));
                DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE ESTACIONES DE SERVICIO, GARAGES, PLAYAS DE ESTACIONAMIENTO Y LAVADEROS AUTOMATICOS',
                            'description' => ' O. S. DEL PERSONAL DE ESTACIONES DE SERVICIO, GARAGES, PLAYAS DE ESTACIONAMIENTO Y LAVADEROS AUTOMATICOS'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE FARMACIA',
                            'description' => ' O. S. DEL PERSONAL DE FARMACIA'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE LA FERIA INFANTIL',
                            'description' => ' O. S. DEL PERSONAL DE LA FERIA INFANTIL'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE FERMOLAC',
                            'description' => ' O. S. DEL PERSONAL DE FERMOLAC'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE FERROVIARIOS',
                            'description' => 'O. S. DE FERROVIARIOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL FIBROCEMENTO',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL FIBROCEMENTO'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA FIDEERA',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA FIDEERA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA FORESTAL DE SANTIAGO DEL ESTERO',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA FORESTAL DE SANTIAGO DEL ESTERO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL FOSFORO',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL FOSFORO'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DE FOTOGRAFOS',
                            'description' => ' O. S. DE FOTOGRAFOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD FRUCTICOLA',
                            'description' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD FRUCTICOLA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE MANIPULEO, EMPAQUE Y EXPEDICION DE FRUTA FRESCA Y HORTALIZAS DE CUYO',
                            'description' => 'O. S. DEL PERSONAL DE MANIPULEO, EMPAQUE Y EXPEDICION DE FRUTA FRESCA Y HORTALIZAS DE CUYO'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DE FUTBOLISTAS',
                            'description' => ' O. S. DE FUTBOLISTAS'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DE TECNICOS DE FUTBOL',
                            'description' => ' O. S. DE TECNICOS DE FUTBOL'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL GASTRONOMICO',
                            'description' => ' O. S. DEL PERSONAL GASTRONOMICO'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL SUPERIOR DE GOOD YEAR ARGENTINA',
                            'description' => 'O. S. DEL PERSONAL SUPERIOR DE GOOD YEAR ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL GRAFICO',
                            'description' => 'O. S. DEL PERSONAL GRAFICO'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL GRAFICO DE CORRIENTES',
                            'description' => ' O. S. DEL PERSONAL GRAFICO DE CORRIENTES'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE GUINCHEROS Y MAQUINISTAS DE GRUAS MOVILES',
                            'description' => 'O. S. DE GUINCHEROS Y MAQUINISTAS DE GRUAS MOVILES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE CONSIGNATARIOS DEL MERCADO NACIONAL DE HACIENDA DE LINIERS',
                            'description' => 'O. S. DEL PERSONAL DE CONSIGNATARIOS DEL MERCADO NACIONAL DE HACIENDA DE LINIERS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL HIELO Y MERCADOS PARTICULARES',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL HIELO Y MERCADOS PARTICULARES'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE LOS HIPODROMOS DE BUENOS AIRES Y SAN ISIDRO',
                            'description' => ' O. S. DEL PERSONAL DE LOS HIPODROMOS DE BUENOS AIRES Y SAN ISIDRO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL MENSUALIZADO DEL JOCKEY CLUB DE BUENOS AIRES Y LOS HIPODROMOS DE PALERMO Y SAN ISIDRO',
                            'description' => 'O. S. DEL PERSONAL MENSUALIZADO DEL JOCKEY CLUB DE BUENOS AIRES Y LOS HIPODROMOS DE PALERMO Y SAN ISIDRO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE IMPRENTA, DIARIOS Y AFINES',
                            'description' => 'O. S. DEL PERSONAL DE IMPRENTA, DIARIOS Y AFINES'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DEL INGENIO SAN PABLO',
                            'description' => 'O. S. DEL PERSONAL DEL INGENIO SAN PABLO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE JABONEROS
',
                            'description' => 'O. S. DEL PERSONAL DE JABONEROS
'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE JARDINER., PARQUISTAS, VIVERISTAS Y FLORIC. DE R.A.',
                            'description' => 'O. S. DE JARDINER., PARQUISTAS, VIVERISTAS Y FLORIC. DE R.A.'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE JOCKEY CLUB DE ROSARIO',
                            'description' => 'O. S. DEL PERSONAL DE JOCKEY CLUB DE ROSARIO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL LADRILLERO',
                            'description' => 'O. S. DEL PERSONAL LADRILLERO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA LADRILLERA A MAQUINA',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA LADRILLERA A MAQUINA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA LECHERA',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA LECHERA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LOCUTORES',
                            'description' => 'O. S. DE LOCUTORES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LA FEDERACION ARGENTINA DE TRABAJADORES DE LUZ Y FUERZA',
                            'description' => 'O. S. DE LA FEDERACION ARGENTINA DE TRABAJADORES DE LUZ Y FUERZA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LOS TRABAJADORES DE LAS EMPRESAS DE ELECTRICIDAD',
                            'description' => 'O. S. DE LOS TRABAJADORES DE LAS EMPRESAS DE ELECTRICIDAD'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LUZ Y FUERZA DE CORDOBA',
                            'description' => 'O. S. DEL PERSONAL DE LUZ Y FUERZA DE CORDOBA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA MADERERA',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA MADERERA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE MAESTRANZA',
                            'description' => 'O. S. DEL PERSONAL DE MAESTRANZA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE MAQUINISTAS DE TEATRO Y TELEVISION',
                            'description' => 'O. S. DE MAQUINISTAS DE TEATRO Y TELEVISION'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE CAPITANES DE ULTRAMAR Y OFICIALES DE LA MARINA MERCANTE',
                            'description' => 'O. S. DE CAPITANES DE ULTRAMAR Y OFICIALES DE LA MARINA MERCANTE'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. CAPITANES BAQUEANOS FLUVIALES DE LA MARINA MERCANTE',
                            'description' => 'O. S. CAPITANES BAQUEANOS FLUVIALES DE LA MARINA MERCANTE'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE EMPLEADOS DE LA MARINA MERCANTE',
                            'description' => 'O. S. DE EMPLEADOS DE LA MARINA MERCANTE'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE ENCARGADOS APUNTADORES MARITIMOS',
                            'description' => 'O. S. DE ENCARGADOS APUNTADORES MARITIMOS'));
                DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL MARITIMO',
                            'description' => 'O. S. DEL PERSONAL MARITIMO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL SINDICATO DE MECANICOS Y AFINES DEL TRANSPORTE AUTOMOTOR',
                            'description' => 'O. S. DEL SINDICATO DE MECANICOS Y AFINES DEL TRANSPORTE AUTOMOTOR'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL SUPERIOR DE MERCEDES BENZ ARGENTINA',
                            'description' => 'O. S. DEL PERSONAL SUPERIOR DE MERCEDES BENZ ARGENTINA'));
                DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LA UNION OBRERA METALURGICO DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DE LA UNION OBRERA METALURGICO DE LA REPUBLICA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DE LOS SUPERVISORES DE LA INDUSTRIA METALMECANICA DE LA REPUBLICA ARGENTINA',
                            'description' => ' O. S. DE LOS SUPERVISORES DE LA INDUSTRIA METALMECANICA DE LA REPUBLICA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE MICROS Y OMNIBUS DE MENDOZA',
                            'description' => 'O. S. DEL PERSONAL DE MICROS Y OMNIBUS DE MENDOZA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LA ACTIVIDAD MINERA',
                            'description' => 'O. S. DE LA ACTIVIDAD MINERA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. MODELOS ARGENTINOS',
                            'description' => 'O. S. MODELOS ARGENTINOS'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE LA INDUSTRIA MOLINERA',
                            'description' => ' O. S. DEL PERSONAL DE LA INDUSTRIA MOLINERA'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL MOSAISTA',
                            'description' => ' O. S. DEL PERSONAL MOSAISTA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE MUSICOS',
                            'description' => 'O. S. DE MUSICOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE MUSICOS DE CUYO',
                            'description' => 'O. S. DE MUSICOS DE CUYO'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE MUSICOS DE MAR DEL PLATA',
                            'description' => 'O. S. DE MUSICOS DE MAR DEL PLATA'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DE JEFES Y OFICIALES NAVALES DE RADIOCOMUNICACIONES',
                            'description' => ' O. S. DE JEFES Y OFICIALES NAVALES DE RADIOCOMUNICACIONES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE JEFES Y OFICIALES MAQUINISTAS NAVALES',
                            'description' => 'O. S. DE JEFES Y OFICIALES MAQUINISTAS NAVALES'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL NAVAL',
                            'description' => 'O. S. DEL PERSONAL NAVAL'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL NEUMATICO',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL NEUMATICO'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE LA INDUSTRIA NAVAL',
                            'description' => ' O. S. DEL PERSONAL DE LA INDUSTRIA NAVAL'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE COMISARIOS NAVALES',
                            'description' => 'O. S. DE COMISARIOS NAVALES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE PANADERIAS',
                            'description' => 'O. S. DEL PERSONAL DE PANADERIAS'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DE PANADEROS, PASTELEROS Y FACTUREROS DE ENTRE RIOS',
                            'description' => ' O. S. DE PANADEROS, PASTELEROS Y FACTUREROS DE ENTRE RIOS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DEL PAPEL, CARTON Y QUIMICOS',
                            'description' => 'O. S. DEL PERSONAL DEL PAPEL, CARTON Y QUIMICOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LA INDUSTRIA DE PASTAS ALIMENTICIAS',
                            'description' => 'O. S. DE LA INDUSTRIA DE PASTAS ALIMENTICIAS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. TRABAJ. PAST, CONFIT, PIZZEROS, HELAD. Y ALFAJ. DE LA R.A.',
                            'description' => 'O. S. TRABAJ. PAST, CONFIT, PIZZEROS, HELAD. Y ALFAJ. DE LA R.A.'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE PATRONES DE CABOTAJE DE RIOS Y PUERTOS',
                            'description' => 'O. S. DE PATRONES DE CABOTAJE DE RIOS Y PUERTOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE PELETEROS',
                            'description' => 'O. S. DE PELETEROS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE PELUQUERIAS, ESTETICA Y AFINES',
                            'description' => 'O. S. DEL PERSONAL DE PELUQUERIAS, ESTETICA Y AFINES'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE OFICIALES PELUQUEROS Y PEINADORES',
                            'description' => 'O. S. DE OFICIALES PELUQUEROS Y PEINADORES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. OFICIALES PELUQUEROS Y PEINADORES DE ROSARIO',
                            'description' => 'O. S. OFICIALES PELUQUEROS Y PEINADORES DE ROSARIO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD PERFUMISTA',
                            'description' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD PERFUMISTA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE TRABAJADORES DE PRENSA DE BUENOS AIRES',
                            'description' => 'O. S. DE TRABAJADORES DE PRENSA DE BUENOS AIRES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUST. DEL PESCADO DE MAR DEL PLATA',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUST. DEL PESCADO DE MAR DEL PLATA'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DE PETROLEROS',
                            'description' => ' O. S. DE PETROLEROS'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PETROLEO Y GAS PRIVADO',
                            'description' => ' O. S. DEL PETROLEO Y GAS PRIVADO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE PETROLEROS DE CORDOBA',
                            'description' => 'O. S. DE PETROLEROS DE CORDOBA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA PETROQUIMICA',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA PETROQUIMICA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE PILOTOS DE LINEAS AEREAS COMERCIALES Y REGULARES',
                            'description' => 'O. S. DE PILOTOS DE LINEAS AEREAS COMERCIALES Y REGULARES'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE FABRICAS DE PINTURA',
                            'description' => ' O. S. DEL PERSONAL DE FABRICAS DE PINTURA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE ACADEMIAS PITMAN',
                            'description' => 'O. S. DEL PERSONAL DE ACADEMIAS PITMAN'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE LA INDUSTRIA DEL PLASTICO',
                            'description' => ' O. S. DEL PERSONAL DE LA INDUSTRIA DEL PLASTICO'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DE CAPATACES ESTIBADORES PORTUARIOS',
                            'description' => ' O. S. DE CAPATACES ESTIBADORES PORTUARIOS'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DE PORTUARIOS ARGENTINOS',
                            'description' => ' O. S. DE PORTUARIOS ARGENTINOS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PORTUARIOS DE BAHIA BLANCA',
                            'description' => 'O. S. PORTUARIOS DE BAHIA BLANCA'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. PORTUARIOS DE NECOCHEA Y QUEQUEN',
                            'description' => ' O. S. PORTUARIOS DE NECOCHEA Y QUEQUEN'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PORTUARIOS DE ROSARIO',
                            'description' => 'O. S. PORTUARIOS DE ROSARIO'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PORTUARIOS DE SAN LORENZO',
                            'description' => 'O. S. PORTUARIOS DE SAN LORENZO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PORTUARIOS DE PUERTO SAN MARTIN Y BELLA VISTA',
                            'description' => 'O. S. PORTUARIOS DE PUERTO SAN MARTIN Y BELLA VISTA'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. PORTUARIOS PUERTO SAN NICOLAS',
                            'description' => ' O. S. PORTUARIOS PUERTO SAN NICOLAS'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. PORTUARIOS DE SAN PEDRO',
                            'description' => ' O. S. PORTUARIOS DE SAN PEDRO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PORTUARIOS DE SANTA FE',
                            'description' => 'O. S. PORTUARIOS DE SANTA FE'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. PORTUARIOS DE VILLA CONSTITUCIÓN',
                            'description' => ' O. S. PORTUARIOS DE VILLA CONSTITUCIÓN'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE PRENSA DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DEL PERSONAL DE PRENSA DE LA REPUBLICA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE PRENSA DE BAHIA BLANCA',
                            'description' => 'O. S. DEL PERSONAL DE PRENSA DE BAHIA BLANCA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE PRENSA DE LA PROVINCIA DEL CHACO',
                            'description' => 'O. S. DEL PERSONAL DE PRENSA DE LA PROVINCIA DEL CHACO'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE PRENSA DE MAR DEL PLATA',
                            'description' => 'O. S. DEL PERSONAL DE PRENSA DE MAR DEL PLATA'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE PRENSA DE MENDOZA',
                            'description' => ' O. S. DEL PERSONAL DE PRENSA DE MENDOZA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. EMPLEADOS DE PRENSA DE CORDOBA',
                            'description' => 'O. S. EMPLEADOS DE PRENSA DE CORDOBA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE PROFESIONALES DE LA OS DEL PERSONAL DEL PAPEL, CARTON Y QUIMICOS',
                            'description' => 'O. S. DE PROFESIONALES DE LA OS DEL PERSONAL DEL PAPEL, CARTON Y QUIMICOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE AGENTES DE PROPAGANDA MEDICA DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DE AGENTES DE PROPAGANDA MEDICA DE LA REPUBLICA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE AGENTES DE PROPAGANDA MEDICA DE CORDOBA',
                            'description' => 'O. S. DE AGENTES DE PROPAGANDA MEDICA DE CORDOBA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE AGENTES DE PROPAGANDA MEDICA DE ENTRE RIOS',
                            'description' => 'O. S. DE AGENTES DE PROPAGANDA MEDICA DE ENTRE RIOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE AGENTES DE PROPAGANDA MEDICA DE ROSARIO',
                            'description' => 'O. S. DE AGENTES DE PROPAGANDA MEDICA DE ROSARIO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA PUBLICIDAD',
                            'description' => 'O. S. DEL PERSONAL DE LA PUBLICIDAD'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE INDUSTRIAS QUIMICAS Y PETROQUIMICAS',
                            'description' => ' O. S. DEL PERSONAL DE INDUSTRIAS QUIMICAS Y PETROQUIMICAS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE RECIBIDORES DE GRANOS Y ANEXOS',
                            'description' => 'O. S. DE RECIBIDORES DE GRANOS Y ANEXOS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE RECOLECCION Y BARRIDO DE ROSARIO',
                            'description' => 'O. S. DEL PERSONAL DE RECOLECCION Y BARRIDO DE ROSARIO'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE REFINERIAS DE MAIZ',
                            'description' => ' O. S. DEL PERSONAL DE REFINERIAS DE MAIZ'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LA INDUSTRIA DE MATERIALES REFRACTARIOS Y AFINES',
                            'description' => 'O. S. DE LA INDUSTRIA DE MATERIALES REFRACTARIOS Y AFINES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE RELOJEROS Y JOYEROS',
                            'description' => 'O. S. DE RELOJEROS Y JOYEROS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL RURAL Y ESTIBADORES DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DEL PERSONAL RURAL Y ESTIBADORES DE LA REPUBLICA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA SALINERA',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA SALINERA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA SANIDAD ARGENTINA',
                            'description' => 'O. S. DEL PERSONAL DE LA SANIDAD ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE INSTALACIONES SANITARIAS',
                            'description' => 'O. S. DEL PERSONAL DE INSTALACIONES SANITARIAS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE SEGURIDAD COMERCIAL, INDUSTRIAL E INVESTIGACIONES PRIVADAS',
                            'description' => 'O. S. DEL PERSONAL DE SEGURIDAD COMERCIAL, INDUSTRIAL E INVESTIGACIONES PRIVADAS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DEL SEGURO',
                            'description' => 'O. S. DEL PERSONAL DEL SEGURO'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE SERENOS DE BUQUES',
                            'description' => 'O. S. DE SERENOS DE BUQUES'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DE TECNICOS, PROFESIONALES, EMPLEADOS Y SUPERVISORES DE INFORMATICA TELECOMUNICACIONES. S. A. Y CIMET S.A.',
                            'description' => ' O. S. DE TECNICOS, PROFESIONALES, EMPLEADOS Y SUPERVISORES DE INFORMATICA TELECOMUNICACIONES. S. A. Y CIMET S.A.'));

                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE STANDARD ELECTRIC',
                            'description' => 'O. S. DEL PERSONAL DE STANDARD ELECTRIC'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE SUPERVISION DE LA EMPRESA SUBTERRANEOS DE BUENOS AIRES',
                            'description' => 'O. S. DEL PERSONAL DE SUPERVISION DE LA EMPRESA SUBTERRANEOS DE BUENOS AIRES'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE DIRECCION DE LA EMPRESA SUBTERRANEOS BUENOS AIRES',
                            'description' => 'O. S. DEL PERSONAL DE DIRECCION DE LA EMPRESA SUBTERRANEOS BUENOS AIRES'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE LA INDUSTRIA DEL TABACO',
                            'description' => ' O. S. DEL PERSONAL DE LA INDUSTRIA DEL TABACO'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE EMPLEADOS DEL TABACO',
                            'description' => 'O. S. DE EMPLEADOS DEL TABACO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LAS TELECOMUNICACIONES DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DEL PERSONAL DE LAS TELECOMUNICACIONES DE LA REPUBLICA ARGENTINA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE TRABAJADORES DE LAS COMUNICACIONES (OSTRAC)',
                            'description' => 'O. S. DEL PERSONAL DE TELEVISION'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA TEXTIL',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA TEXTIL'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE EMPLEADOS TEXTILES Y AFINES',
                            'description' => 'O. S. DE EMPLEADOS TEXTILES Y AFINES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE TINTOREROS, SOMBREREROS Y LAVADEROS',
                            'description' => 'O. S. DE TINTOREROS, SOMBREREROS Y LAVADEROS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL TRACTOR',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL TRACTOR'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL TRANSPORTE AUTOMOTOR DE ROSARIO',
                            'description' => 'O. S. DEL TRANSPORTE AUTOMOTOR DE ROSARIO'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LA INDUSTRIA DEL TRANSPORTE AUTOMOTOR DE CORDOBA',
                            'description' => 'O. S. DE LA INDUSTRIA DEL TRANSPORTE AUTOMOTOR DE CORDOBA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE CONDUCTORES DE TRANSPORTE COLECTIVO DE PASAJEROS',
                            'description' => 'O. S. DE CONDUCTORES DE TRANSPORTE COLECTIVO DE PASAJEROS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD DEL TURF',
                            'description' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD DEL TURF'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. CONDUCTORES DE TAXIS DE CORDOBA',
                            'description' => 'O. S. CONDUCTORES DE TAXIS DE CORDOBA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE VAREADORES',
                            'description' => 'O. S. DE VAREADORES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL VESTIDO Y AFINES',
                            'description' => 'O. S. DEL PERSONAL DE LA INDUSTRIA DEL VESTIDO Y AFINES'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE VIAJANTES VENDEDORES DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DE VIAJANTES VENDEDORES DE LA REPUBLICA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE VIAJANTES DE COMERCIO',
                            'description' => 'O. S. DE VIAJANTES DE COMERCIO'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD VIAL',
                            'description' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD VIAL'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DE EMPLEADOS DE LA INDUSTRIA DEL VIDRIO',
                            'description' => ' O. S. DE EMPLEADOS DE LA INDUSTRIA DEL VIDRIO'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE LA INDUSTRIA DEL VIDRIO',
                            'description' => ' O. S. DEL PERSONAL DE LA INDUSTRIA DEL VIDRIO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD VITIVINICOLA',
                            'description' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD VITIVINICOLA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE VIALIDAD NACIONAL',
                            'description' => 'O. S. DEL PERSONAL DE VIALIDAD NACIONAL'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE VIGILANCIA Y SEGURIDAD COMERCIAL, INDUSTRIAL E INVESTIGACIONES PRIVADAS DE CORDOBA',
                            'description' => 'O. S. DEL PERSONAL DE VIGILANCIA Y SEGURIDAD COMERCIAL, INDUSTRIAL E INVESTIGACIONES PRIVADAS DE CORDOBA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL. ESTACIONES DE SERVICIO, GARAGES, PLAYAS Y LAVADEROS AUTOMATICOS DE LA PROVINCIA DE SANTA FE',
                            'description' => 'O. S. DEL PERSONAL. ESTACIONES DE SERVICIO, GARAGES, PLAYAS Y LAVADEROS AUTOMATICOS DE LA PROVINCIA DE SANTA FE'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PARA EL PERSONAL DE ESTACIONES DE SERVICIO, GARAGES, PLAYAS DE ESTACIONAMIENTO, LAVADEROS AUTOMATICOS Y GOMERIAS DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. PARA EL PERSONAL DE ESTACIONES DE SERVICIO, GARAGES, PLAYAS DE ESTACIONAMIENTO, LAVADEROS AUTOMATICOS Y GOMERIAS DE LA REPUBLICA ARGENTINA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE TALLERISTAS A DOMICILIO',
                            'description' => 'O. S. DE TALLERISTAS A DOMICILIO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE BAÑEROS Y AFINES DEL PARTIDO DE GENERAL PUEYRREDON',
                            'description' => 'O. S. DE BAÑEROS Y AFINES DEL PARTIDO DE GENERAL PUEYRREDON'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE SOCIEDADES DE AUTORES Y AFINES',
                            'description' => 'O. S. DEL PERSONAL DE SOCIEDADES DE AUTORES Y AFINES'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE PRENSA DE ROSARIO',
                            'description' => ' O. S. DEL PERSONAL DE PRENSA DE ROSARIO'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE PRENSA DE TUCUMAN',
                            'description' => 'O. S. DEL PERSONAL DE PRENSA DE TUCUMAN'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE TRABAJADORES DE PERKINS ARGENTINA S.A.I.C.',
                            'description' => 'O. S. DE TRABAJADORES DE PERKINS ARGENTINA S.A.I.C.'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL GUARDAVIDAS Y AFINES DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DEL PERSONAL GUARDAVIDAS Y AFINES DE LA REPUBLICA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE VENDEDORES AMBULANTES DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DE VENDEDORES AMBULANTES DE LA REPUBLICA ARGENTINA'));
                                 DB::table('socworks')
                ->insert(array('name' => '. S. DE BOXEADORES AGREMIADOS DE LA REPUBLICA ARGENTINA',
                            'description' => '. S. DE BOXEADORES AGREMIADOS DE LA REPUBLICA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE EMBALADORES, DESCARTADORES Y ALAMBRADORES SAN PEDRO',
                            'description' => 'O. S. DE EMBALADORES, DESCARTADORES Y ALAMBRADORES SAN PEDRO'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE EMPLEADOS Y OBREROS GASTRONOMICOS DE TUCUMAN',
                            'description' => 'O. S. DE EMPLEADOS Y OBREROS GASTRONOMICOS DE TUCUMAN'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PARA EL PERSONAL DE EMPRESA DE LIMPIEZA, SERVICIOS DE MENDOZA',
                            'description' => 'O. S. PARA EL PERSONAL DE EMPRESA DE LIMPIEZA, SERVICIOS DE MENDOZA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LOS TRABAJADORES DE LA INDUSTRIA DEL GAS',
                            'description' => 'O. S. DE LOS TRABAJADORES DE LA INDUSTRIA DEL GAS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE TRABAJADORES DE LA INDUSTRIA AVICOLA Y AFINES DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DE TRABAJADORES DE LA INDUSTRIA AVICOLA Y AFINES DE LA REPUBLICA ARGENTINA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE ARBITROS DE LA ASOCIACION DEL FUTBOL ARGENTINO',
                            'description' => 'O. S. DE ARBITROS DE LA ASOCIACION DEL FUTBOL ARGENTINO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LA FEDERACION NACIONAL DE SINDICATOS DE CONDUCTORES DE TAXIS',
                            'description' => 'O. S. DE LA FEDERACION NACIONAL DE SINDICATOS DE CONDUCTORES DE TAXIS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LOS PROFESIONALES UNIVERSITARIOS DEL AGUA Y LA ENERGIA ELECTRICA',
                            'description' => 'O. S. DE LOS PROFESIONALES UNIVERSITARIOS DEL AGUA Y LA ENERGIA ELECTRICA'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DE CAPITANES, PILOTOS Y PATRONES DE PESCA (VER 505)',
                            'description' => ' O. S. DE CAPITANES, PILOTOS Y PATRONES DE PESCA (VER 505)'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. FEDERAL DE LA FEDERACION DE TRABAJADORES OBRAS SANITARIAS',
                            'description' => 'O. S. FEDERAL DE LA FEDERACION DE TRABAJADORES OBRAS SANITARIAS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PARA EL PERSONAL DE OBRAS Y SERVICIOS SANITARIOS (VER 001003)',
                            'description' => 'O. S. PARA EL PERSONAL DE OBRAS Y SERVICIOS SANITARIOS (VER 001003)'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LA FEDERACION ARGENTINA DEL TRABAJADOR DE LAS UNIVERSIDADES NACIONALES',
                            'description' => 'O. S. DE LA FEDERACION ARGENTINA DEL TRABAJADOR DE LAS UNIVERSIDADES NACIONALES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL SUPERIOR DE LA INDUSTRIA DEL GAS Y AFINES',
                            'description' => 'O. S. DEL PERSONAL SUPERIOR DE LA INDUSTRIA DEL GAS Y AFINES'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. UNION PERSONAL DE LA UNION DE TRABAJADORES CIVIL DE LA NACION',
                            'description' => 'O. S. UNION PERSONAL DE LA UNION DE TRABAJADORES CIVIL DE LA NACION'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL JERARQUICO DEL AGUA Y LA ENERGIA',
                            'description' => 'O. S. DEL PERSONAL JERARQUICO DEL AGUA Y LA ENERGIA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. ARBITROS DEPORTIVOS DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. ARBITROS DEPORTIVOS DE LA REPUBLICA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LA FEDERACION GREMIAL DE LA INDUSTRIA DE LA CARNE Y SUS DERIVADOS',
                            'description' => 'O. S. DE LA FEDERACION GREMIAL DE LA INDUSTRIA DE LA CARNE Y SUS DERIVADOS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LOS TRABAJADORES DE LA EDUCACION PRIVADA',
                            'description' => 'O. S. DE LOS TRABAJADORES DE LA EDUCACION PRIVADA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LOS EMPLEADOS DE COMERCIO Y ACTIVIDADES CIVILES',
                            'description' => 'O. S. DE LOS EMPLEADOS DE COMERCIO Y ACTIVIDADES CIVILES'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. BANCARIA ARGENTINA',
                            'description' => 'O. S. BANCARIA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. MEDICA AVELLANEDA',
                            'description' => 'O. S. MEDICA AVELLANEDA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LA CONFEDERACION DE OBREROS Y EMPLEADOS MUNICIPALES ARGENTINA (O.S.C.O.E.M.A)',
                            'description' => 'O. S. DE LA CONFEDERACION DE OBREROS Y EMPLEADOS MUNICIPALES ARGENTINA (O.S.C.O.E.M.A)'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE INDUSTRIAS QUIMICAS Y PETROQUIMICAS DE ZARATE CAMPANA',
                            'description' => 'O. S. DEL PERSONAL DE INDUSTRIAS QUIMICAS Y PETROQUIMICAS DE ZARATE CAMPANA'));
                 /*              DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD AZUCARERA TUCUMANA',
                            'description' => 'O. S. DEL PERSONAL DE LA ACTIVIDAD AZUCARERA TUCUMANA'));*/
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE CONDUCT. DE REMISES Y AUTOS AL INSTANTE Y AFINES',
                            'description' => 'O. S. DE CONDUCT. DE REMISES Y AUTOS AL INSTANTE Y AFINES'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LOS MEDICOS DE LA CIUDAD DE BUENOS AIRES',
                            'description' => 'O. S. DE LOS MEDICOS DE LA CIUDAD DE BUENOS AIRES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE TRABAJADORES DE ESTACIONES DE SERVICIO (OSTES)',
                            'description' => 'O. S. DE TRABAJADORES DE ESTACIONES DE SERVICIO (OSTES)'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE TELECOMUNICACIONES SINDICATO BUENOS AIRES (OSPETELCO)',
                            'description' => 'O. S. DEL PERSONAL DE TELECOMUNICACIONES SINDICATO BUENOS AIRES (OSPETELCO)'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE MANDOS MEDIOS DE TELECOMUNICACIONES EN LA REPUBLICA ARGENTINA Y MERCOSUR (OSMMEDT)',
                            'description' => 'O. S. DE MANDOS MEDIOS DE TELECOMUNICACIONES EN LA REPUBLICA ARGENTINA Y MERCOSUR (OSMMEDT)'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE TRABAJADORES VIALES Y AFINES DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DE TRABAJADORES VIALES Y AFINES DE LA REPUBLICA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE OBREROS Y EMPLEADOS TINTOREROS SOMBREREROS Y LAVADEROS DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DE OBREROS Y EMPLEADOS TINTOREROS SOMBREREROS Y LAVADEROS DE LA REPUBLICA ARGENTINA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O.S. DE LA SECRETARIA DE AGRICULTURA, GANADERIA Y PESCA',
                            'description' => 'O.S. DE LA SECRETARIA DE AGRICULTURA, GANADERIA Y PESCA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. CENTRO REGIONAL DE AGUAS SUBTERRANEAS',
                            'description' => 'O. S. CENTRO REGIONAL DE AGUAS SUBTERRANEAS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LAS SECRETARIAS DE INDUSTRIA Y COMERCIO EXTERIOR, COMERCIO INTERIOR Y MINERIA',
                            'description' => 'O. S. DE LAS SECRETARIAS DE INDUSTRIA Y COMERCIO EXTERIOR, COMERCIO INTERIOR Y MINERIA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE LA EMPRESA NACIONAL DE CORREOS Y TELEGRAFOS S.A. Y DE LAS COMUNICACIONES DE LA REPUBLICA ARGENTINA',
                            'description' => 'O. S. DEL PERSONAL DE LA EMPRESA NACIONAL DE CORREOS Y TELEGRAFOS S.A. Y DE LAS COMUNICACIONES DE LA REPUBLICA ARGENTINA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL MINISTERIO DE DEFENSA',
                            'description' => 'O. S. DEL MINISTERIO DE DEFENSA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. FLOTA FLUVIAL DEL ESTADO ARGENTINO',
                            'description' => 'O. S. FLOTA FLUVIAL DEL ESTADO ARGENTINO'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. GAS DEL ESTADO',
                            'description' => 'O. S. GAS DEL ESTADO'));
                 DB::table('socworks')
                ->insert(array('name' => 'DIRECCION GENERAL DE: "O. S. DEL MINISTERIO DEL INTERIOR',
                            'description' => 'DIRECCION GENERAL DE: "O. S. DEL MINISTERIO DEL INTERIOR'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL MINISTERIO DE JUSTICIA',
                            'description' => 'O. S. DEL MINISTERIO DE JUSTICIA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE EMPRESA LINEAS MARITIMAS ARGENTINAS',
                            'description' => 'O. S. DE EMPRESA LINEAS MARITIMAS ARGENTINAS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'DIRECCION GENERAL DE: "O. S. DEL MINISTERIO DE RELACIONES EXTERIORES Y CULTO',
                            'description' => 'DIRECCION GENERAL DE: "O. S. DEL MINISTERIO DE RELACIONES EXTERIORES Y CULTO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S DEL MINISTERIO DE OBRAS Y SERVICIOS PUBLICOS',
                            'description' => 'O. S DEL MINISTERIO DE OBRAS Y SERVICIOS PUBLICOS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'DIRECCION DE: "O. S. DE LA EMPRESA NACIONAL DE TELECOMUNICACIONES',
                            'description' => 'DIRECCION DE: "O. S. DE LA EMPRESA NACIONAL DE TELECOMUNICACIONES'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DE LA UNIVERSIDAD DE BUENOS ARIES',
                            'description' => ' O. S. DE LA UNIVERSIDAD DE BUENOS ARIES'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DIRECCION NACIONAL DE VIALIDAD',
                            'description' => 'O. S. DIRECCION NACIONAL DE VIALIDAD'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. YACIMIENTOS CARBONIFEROS FISCALES',
                            'description' => 'O. S. YACIMIENTOS CARBONIFEROS FISCALES'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. YACIMIENTOS PETROLIFEROS FISCALES',
                            'description' => 'O. S. YACIMIENTOS PETROLIFEROS FISCALES'));
                 DB::table('socworks')
                ->insert(array('name' => 'INSTITUTO NACIONAL DE OBRAS SOCIALES PARA EL PERSONAL DE LA ADMINISTRACION PUBLICA NACIONAL Y SUS ENTES AUTARQUICOS Y DESCENTRALIZADOS',
                            'description' => 'INSTITUTO NACIONAL DE OBRAS SOCIALES PARA EL PERSONAL DE LA ADMINISTRACION PUBLICA NACIONAL Y SUS ENTES AUTARQUICOS Y DESCENTRALIZADOS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. ANILSUD',
                            'description' => 'O. S. ANILSUD'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. BOROQUIMICA S.A.M.I.C.A.F.',
                            'description' => 'O. S. BOROQUIMICA S.A.M.I.C.A.F.'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE LA EMPRESA PRIVADA CELULOSA ARGENTINA S.A.',
                            'description' => 'O. S. DE LA EMPRESA PRIVADA CELULOSA ARGENTINA S.A.'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. CORPORACION CEMENTERA ARGENTINA',
                            'description' => 'O. S. CORPORACION CEMENTERA ARGENTINA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE CERAMICA SAN LORENZO',
                            'description' => 'O. S. DEL PERSONAL DE CERAMICA SAN LORENZO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. CERAS JOHNSON',
                            'description' => 'O. S. CERAS JOHNSON'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. CALILEGUA S.A.A.I.C.',
                            'description' => 'O. S. CALILEGUA S.A.A.I.C.'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DESTILERIAS SAN IGNACIO S.A.I.C.',
                            'description' => 'O. S. DESTILERIAS SAN IGNACIO S.A.I.C.'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DUNLOP ARGENTINA LIMITADA',
                            'description' => 'O. S. DUNLOP ARGENTINA LIMITADA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DUPERIAL ORBEA',
                            'description' => 'O. S. DUPERIAL ORBEA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. COMPAÑÍA EMBOTELLADORA ARGENTINA',
                            'description' => 'O. S. COMPAÑÍA EMBOTELLADORA ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. ELECTROCLOR S.C.A',
                            'description' => 'O. S. ELECTROCLOR S.C.A'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. MUTUALIDAD EMPLEADOS FIRESTONE',
                            'description' => 'O. S. MUTUALIDAD EMPLEADOS FIRESTONE'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. AUTOLATINA ARGENTINA S.A.',
                            'description' => 'O. S. AUTOLATINA ARGENTINA S.A.'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE ALLIED DOMECD ARGENTINA. S.A (ANTERIOR DENOMINACION: "O. S. HIRAN WALKER S.A )',
                            'description' => 'O. S. DE ALLIED DOMECD ARGENTINA. S.A (ANTERIOR DENOMINACION: "O. S. HIRAN WALKER S.A )'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. SOCIEDAD MINERA HIERRO PATAGONICOS DE SIERRA GRANDE',
                            'description' => 'O. S. SOCIEDAD MINERA HIERRO PATAGONICOS DE SIERRA GRANDE'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE SERVICIOS ASISTENCIALES DE LA COMPAÑÍA ITALO ARGENTINA DE ELECTRICIDAD',
                            'description' => 'O. S. DE SERVICIOS ASISTENCIALES DE LA COMPAÑÍA ITALO ARGENTINA DE ELECTRICIDAD'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. INGENIO RIO GRANDE S.A.',
                            'description' => 'O. S. INGENIO RIO GRANDE S.A.'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PARA DIRECTIVOS, TECNICOS Y EMPLEADOS DE JOHN DEERE ARGENTINA',
                            'description' => 'O. S. PARA DIRECTIVOS, TECNICOS Y EMPLEADOS DE JOHN DEERE ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. LEDESMA S.A.A.I.',
                            'description' => 'O. S. LEDESMA S.A.A.I.'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. COMPAÑIA MINERA AGUILAR S.A.',
                            'description' => 'O. S. COMPAÑIA MINERA AGUILAR S.A.'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. MOLINOS RIO DE LA PLATA',
                            'description' => ' O. S. MOLINOS RIO DE LA PLATA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PASA PETROQUIMICA ARGENTINA S.A.',
                            'description' => 'O. S. PASA PETROQUIMICA ARGENTINA S.A.'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. COOPERATIVA DE ASISTENCIA MUTUA Y TURISMO DEL PERSONAL DE LAS SOCIEDADES PIRELLI LIMITADA',
                            'description' => 'O. S. COOPERATIVA DE ASISTENCIA MUTUA Y TURISMO DEL PERSONAL DE LAS SOCIEDADES PIRELLI LIMITADA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. COMPAÑÍA QUIMICA S.A.',
                            'description' => 'O. S. COMPAÑÍA QUIMICA S.A.'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. REFINERIAS DE MAIZ S.A.I.C.F.',
                            'description' => 'O. S. REFINERIAS DE MAIZ S.A.I.C.F.'));
                                 DB::table('socworks')
                ->insert(array('name' => 'DIRECCION GENERAL DE SERVICIOS ASISTENCIALES DE SERVICIOS ELECTRICOS DEL GRAN BUENOS AIRES',
                            'description' => 'DIRECCION GENERAL DE SERVICIOS ASISTENCIALES DE SERVICIOS ELECTRICOS DEL GRAN BUENOS AIRES'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. SUPERCO',
                            'description' => ' O. S. SUPERCO'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. EMPRESA PRIVADA WITCEL S.A.',
                            'description' => 'O. S. EMPRESA PRIVADA WITCEL S.A.'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. CABOT ARGENTINA',
                            'description' => ' O. S. CABOT ARGENTINA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE SHELL - CAPSA',
                            'description' => 'O. S. DEL PERSONAL DE SHELL - CAPSA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSON. DE LA COMPAÑIA GRAL. DE COMBUSTIBLES S.A.',
                            'description' => 'O. S. DEL PERSON. DE LA COMPAÑIA GRAL. DE COMBUSTIBLES S.A.'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE IPAKO S.A.',
                            'description' => 'O. S. DE IPAKO S.A.'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE PBBPOLISUR DE BAHIA BLANCA',
                            'description' => 'O. S. DEL PERSONAL DE PBBPOLISUR DE BAHIA BLANCA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE ARBITRO DEL FUTBOL ARGENTINO',
                            'description' => 'O. S. DE ARBITRO DEL FUTBOL ARGENTINO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE FORD ARGENTINA S.A.',
                            'description' => 'O. S. DE FORD ARGENTINA S.A.'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE VOLKSWAGEN ARGENTINA S.A.',
                            'description' => 'O. S. DE VOLKSWAGEN ARGENTINA S.A.'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE DIRECCION DE LAS EMPRESAS DE LA ALIMENTACION Y DEMAS ACTIVIDADES EMPRESARIAS',
                            'description' => 'O. S. DEL PERSONAL DE DIRECCION DE LAS EMPRESAS DE LA ALIMENTACION Y DEMAS ACTIVIDADES EMPRESARIAS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE DIRECCION DE LA INDUSTRIA AUTOMOTRIZ ARGENTINA',
                            'description' => 'O. S. DEL PERSONAL DE DIRECCION DE LA INDUSTRIA AUTOMOTRIZ ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. COOPERATIVA LIMITADA DE ASISTENCIA MEDICA, FARMACEUTICA, CREDITO Y CONSUMO DEL PERSONAL SUPERIOR DE LA INDUSTRIA DEL CAUCHO Y OTRAS ACTIVIDADES INDUSTRIALES',
                            'description' => 'O. S. COOPERATIVA LIMITADA DE ASISTENCIA MEDICA, FARMACEUTICA, CREDITO Y CONSUMO DEL PERSONAL SUPERIOR DE LA INDUSTRIA DEL CAUCHO Y OTRAS ACTIVIDADES INDUSTRIALES'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DE DIRECCION DE LA INDUSTRIA CERVECERA Y MALTERA',
                            'description' => ' O. S. DEL PERSONAL DE DIRECCION DE LA INDUSTRIA CERVECERA Y MALTERA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. ASOCIACION DEL PERSONAL DE DIRECCION Y JERERQUICO DE LA INDUSTRIA DEL CIGARRILLO',
                            'description' => 'O. S. ASOCIACION DEL PERSONAL DE DIRECCION Y JERERQUICO DE LA INDUSTRIA DEL CIGARRILLO'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL DIRECTIVO DE LA INDUSTRIA DE LA CONSTRUCCION',
                            'description' => ' O. S. DEL PERSONAL DIRECTIVO DE LA INDUSTRIA DE LA CONSTRUCCION'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. CAMARA DE LA INDUSTRIA CURTIDORA ARGENTINA
',
                            'description' => 'O. S. CAMARA DE LA INDUSTRIA CURTIDORA ARGENTINA
'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE EJECUTIVOS. Y DEL PERSONAL DE DIRECCION DE EMPRESAS',
                            'description' => 'O. S. DE EJECUTIVOS. Y DEL PERSONAL DE DIRECCION DE EMPRESAS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. ACCION SOCIAL DE EMPRESARIOS',
                            'description' => 'O. S. ACCION SOCIAL DE EMPRESARIOS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE DIRECCION DE LAS EMPRESAS QUE ACTUAN EN FRUTOS DEL PAIS',
                            'description' => 'O. S. DEL PERSONAL DE DIRECCION DE LAS EMPRESAS QUE ACTUAN EN FRUTOS DEL PAIS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE DIRECCION ALFREDO FORTABAT',
                            'description' => 'O. S. DEL PERSONAL DE DIRECCION ALFREDO FORTABAT'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE DIRECCION DE LA INDUSTRIA METALURGICA Y DEMAS ACTIVIDADES EMPRESARIAS',
                            'description' => 'O. S. DEL PERSONAL DE DIRECCION DE LA INDUSTRIA METALURGICA Y DEMAS ACTIVIDADES EMPRESARIAS'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PARA EL PERSONAL DE DIRECCION DE LA INDUSTRIA MADERERA',
                            'description' => 'O. S. PARA EL PERSONAL DE DIRECCION DE LA INDUSTRIA MADERERA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PARA EL PERSONAL DE DIRECCION DE LA ACTIVIDAD MINERA',
                            'description' => 'O. S. PARA EL PERSONAL DE DIRECCION DE LA ACTIVIDAD MINERA'));
                 DB::table('socworks')
                ->insert(array('name' => 'SOCIACION DE PRESTACIONES SOCIALES PARA EMPRESARIOS Y PERSONAL DE DIRECCION DE EMPRESAS DE LA PRODUCCION, INDUSTRIA, COMERCIO Y SERVICIOS',
                            'description' => 'SOCIACION DE PRESTACIONES SOCIALES PARA EMPRESARIOS Y PERSONAL DE DIRECCION DE EMPRESAS DE LA PRODUCCION, INDUSTRIA, COMERCIO Y SERVICIOS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE DIRECCION DE LA INDUSTRIA PRIVADA DEL PETROLEO',
                            'description' => 'O. S. DEL PERSONAL DE DIRECCION DE LA INDUSTRIA PRIVADA DEL PETROLEO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE DIRECTIVOS Y EMPRESARIOS PEQUEÑOS Y MEDIANOS',
                            'description' => 'O. S. DE DIRECTIVOS Y EMPRESARIOS PEQUEÑOS Y MEDIANOS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. ASOCIACION MUTUAL DEL PERSONAL DE PHILIPS ARGENTINA (AMPAR)',
                            'description' => 'O. S. ASOCIACION MUTUAL DEL PERSONAL DE PHILIPS ARGENTINA (AMPAR)'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE DIRECCION DE PERFUMERIAS E. W. HOPE',
                            'description' => 'O. S. DEL PERSONAL DE DIRECCION DE PERFUMERIAS E. W. HOPE'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE DIRECCION DE LA SANIDAD LUIS PASTEUR',
                            'description' => 'O. S. DEL PERSONAL DE DIRECCION DE LA SANIDAD LUIS PASTEUR'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. ASOCIACION DEL PERSONAL DE DIRECCION DE LA INDUSTRIA SIDERURGICA',
                            'description' => 'O. S. ASOCIACION DEL PERSONAL DE DIRECCION DE LA INDUSTRIA SIDERURGICA'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. MUTUALIDAD INDUSTRIAL TEXTIL ARGENTINA',
                            'description' => ' O. S. MUTUALIDAD INDUSTRIAL TEXTIL ARGENTINAl'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. ASOCIACION DEL PERSONAL SUPERIOR DE LA ORGANIZACIÓN TECHINT',
                            'description' => 'O. S. ASOCIACION DEL PERSONAL SUPERIOR DE LA ORGANIZACIÓN TECHINT'));
                                 DB::table('socworks')
                ->insert(array('name' => ' O. S. PARA EL PERSONAL DE DIRECCION DE LA INDUTRIA VITIVINICOLA Y AFINES',
                            'description' => ' O. S. PARA EL PERSONAL DE DIRECCION DE LA INDUTRIA VITIVINICOLA Y AFINES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. Y.P.F.',
                            'description' => 'O. S. Y.P.F.'));
                                 DB::table('socworks')
                ->insert(array('name' => 'ASOCIACION DE SERVICIOS SOCIALES PARA EMPRESARIOS Y PERSONAL DE DIRECCION DE EMPRESAS DEL COMERCIO, SERVICIOS, PRODUCCION, INDUSTRIA Y CIVIL',
                            'description' => 'ASOCIACION DE SERVICIOS SOCIALES PARA EMPRESARIOS Y PERSONAL DE DIRECCION DE EMPRESAS DEL COMERCIO, SERVICIOS, PRODUCCION, INDUSTRIA Y CIVIL'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE DIRECCION OSDO',
                            'description' => 'O. S. DE DIRECCION OSDO'));
                                 DB::table('socworks')
                ->insert(array('name' => 'INSTITUTO DE SERVICIOS SOCIALES BANCARIOS',
                            'description' => ' INSTITUTO DE SERVICIOS SOCIALES BANCARIOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'INSTITUTO DE OBRA SOCIAL',
                            'description' => 'INSTITUTO DE OBRA SOCIAL'));
                                 DB::table('socworks')
                ->insert(array('name' => 'INSTITUTO DE SERVICIOS SOCIALES PARA EL PERSONAL DE LA INDUSTRIA DE LA CARNE Y AFINES',
                            'description' => 'INSTITUTO DE SERVICIOS SOCIALES PARA EL PERSONAL DE LA INDUSTRIA DE LA CARNE Y AFINES'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PARA EMPLEADOS DE COMERCIO Y ACTIVIDADES CIVILES',
                            'description' => 'O. S. PARA EMPLEADOS DE COMERCIO Y ACTIVIDADES CIVILES'));
                       /*          DB::table('socworks')
                ->insert(array('name' => 'O. S. PARA LA ACTIVIDAD DOCENTE',
                            'description' => 'O. S. PARA LA ACTIVIDAD DOCENTE'));*/
                 DB::table('socworks')
                ->insert(array('name' => ' INSTITUTO DE SERVICIOS SOCIALES PARA EL PERSONAL FERROVIARIO',
                            'description' => ' INSTITUTO DE SERVICIOS SOCIALES PARA EL PERSONAL FERROVIARIO'));
                                 DB::table('socworks')
                ->insert(array('name' => ' INSTITUTO DE OS PARA EL PERSONAL DEL MINISTERIO DE ECONOMIA Y DE OBRA Y SERVICIOS PUBLICOS',
                            'description' => ' INSTITUTO DE OS PARA EL PERSONAL DEL MINISTERIO DE ECONOMIA Y DE OBRA Y SERVICIOS PUBLICOS'));
                 DB::table('socworks')
                ->insert(array('name' => 'INSTITUTO NACIONAL DE SERVICIOS SOCIALES PARA JUBILADOS Y PENSIONADOS',
                            'description' => 'INSTITUTO NACIONAL DE SERVICIOS SOCIALES PARA JUBILADOS Y PENSIONADOS'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PARA EL PERSONAL DE OBRAS SANITARIAS DE LA NACION',
                            'description' => 'O. S. PARA EL PERSONAL DE OBRAS SANITARIAS DE LA NACION'));
                 DB::table('socworks')
                ->insert(array('name' => 'NSTITUTO DE SERVICIOS SOCIALES PARA LAS ACTIVIDADES RURALES Y AFINES',
                            'description' => 'NSTITUTO DE SERVICIOS SOCIALES PARA LAS ACTIVIDADES RURALES Y AFINES'));
                                 DB::table('socworks')
                ->insert(array('name' => 'INSTITUTO DE SERVICIOS SOCIALES PARA EL PERSONAL DE SEGUROS, REASEGUROS, CAPITALIZACION Y AHORRO Y PRESTAMO PARA LA VIVIENDA',
                            'description' => 'INSTITUTO DE SERVICIOS SOCIALES PARA EL PERSONAL DE SEGUROS, REASEGUROS, CAPITALIZACION Y AHORRO Y PRESTAMO PARA LA VIVIENDA'));
                 DB::table('socworks')
                ->insert(array('name' => ' INSTITUTO DE SERVICIOS SOCIALES PARA EL TERRITORIO NACIONAL DE TIERRA DEL FUEGO, ANTARTIDA E ISLAS DEL ATLANTICO SUR',
                            'description' => ' INSTITUTO DE SERVICIOS SOCIALES PARA EL TERRITORIO NACIONAL DE TIERRA DEL FUEGO, ANTARTIDA E ISLAS DEL ATLANTICO SUR'));
                                 DB::table('socworks')
                ->insert(array('name' => ' INSTITUTO DE SERVICIOS SOCIALES PARA EL PERSONAL DE LA INDUSTRIA DEL VIDRIO Y AFINES',
                            'description' => ' INSTITUTO DE SERVICIOS SOCIALES PARA EL PERSONAL DE LA INDUSTRIA DEL VIDRIO Y AFINES'));
                 DB::table('socworks')
                ->insert(array('name' => 'ASOCIACION DE OBRAS SOCIALES DE BELLA VISTA',
                            'description' => 'ASOCIACION DE OBRAS SOCIALES DE BELLA VISTA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'ASOCIACION DE OBRAS SOCIALES DE COMODORO RIVADAVIA',
                            'description' => 'ASOCIACION DE OBRAS SOCIALES DE COMODORO RIVADAVIA'));
                 DB::table('socworks')
                ->insert(array('name' => 'ASOCIACION GUALEGUAYCHU DE OBRAS SOCIALES',
                            'description' => 'ASOCIACION GUALEGUAYCHU DE OBRAS SOCIALES'));
                                 DB::table('socworks')
                ->insert(array('name' => 'ASOCIACION DE OBRAS SOCIALES DE GUALEGUAY',
                            'description' => 'ASOCIACION DE OBRAS SOCIALES DE GUALEGUAY'));
                 DB::table('socworks')
                ->insert(array('name' => 'ASOCIACION DE OBRAS SOCIALES DE MAR DEL PLATA',
                            'description' => 'ASOCIACION DE OBRAS SOCIALES DE MAR DEL PLATA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'ASOCIACION DE OBRAS SOCIALES DE ROSARIO',
                            'description' => 'ASOCIACION DE OBRAS SOCIALES DE ROSARIO'));
                 DB::table('socworks')
                ->insert(array('name' => 'ASOCIACION DE OBRAS SOCIALES DE SAN JORGE',
                            'description' => 'ASOCIACION DE OBRAS SOCIALES DE SAN JORGE'));
                                 DB::table('socworks')
                ->insert(array('name' => 'ASOCIACION DE OBRAS SOCIALES DE TRELEW',
                            'description' => 'ASOCIACION DE OBRAS SOCIALES DE TRELEW'));
                 DB::table('socworks')
                ->insert(array('name' => ' O. S. DEL PERSONAL MUNICIPAL DE AVELLANEDA',
                            'description' => ' O. S. DEL PERSONAL MUNICIPAL DE AVELLANEDA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL MUNICIPAL DE LA MATANZA',
                            'description' => 'O. S. DEL PERSONAL MUNICIPAL DE LA MATANZA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL MUNICIPAL DE TRES DE FEBRERO',
                            'description' => 'O. S. DEL PERSONAL MUNICIPAL DE TRES DE FEBRERO'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL MUNICIPAL DE SANTIAGO DEL ESTERO',
                            'description' => 'O. S. DEL PERSONAL MUNICIPAL DE SANTIAGO DEL ESTERO'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. TRABAJADORES MUNICIPALES DE GRAL. PUEYRREDON',
                            'description' => 'O. S. TRABAJADORES MUNICIPALES DE GRAL. PUEYRREDON'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. ATANOR S.A. MIXTA',
                            'description' => 'O. S. ATANOR S.A. MIXTA'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. FORJA ARGENTINA S.A.I.C.',
                            'description' => 'O. S. FORJA ARGENTINA S.A.I.C.'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. PAPEL MISIONERO S.A.I.F. Y C.',
                            'description' => 'O. S. PAPEL MISIONERO S.A.I.F. Y C.'));
                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DE ACEROS PARANA',
                            'description' => 'O. S. DE ACEROS PARANA'));
                                 DB::table('socworks')
                ->insert(array('name' => 'O. S. DEL PERSONAL DE EMPRESAS FIAT Y EMPRESAS PEUGEOT CITROEN ARGENTINA',
                            'description' => 'O. S. DEL PERSONAL DE EMPRESAS FIAT Y EMPRESAS PEUGEOT CITROEN ARGENTINA'));
                 DB::table('socworks')
                ->insert(array('name' => 'Sin Obra Social',
                            'description' => 'Sin Obra Social'));
                                 DB::table('socworks')
                ->insert(array('name' => 'OBRA SOCIAL DEL PERSONAL DE LA FEDERACION DE SINDICATOS DE LA INDUSTRIA QUIMICAS Y PETROQUIMICAS DE LA REPUBLICA ARGENTINA',
                            'description' => 'OBRA SOCIAL DEL PERSONAL DE LA FEDERACION DE SINDICATOS DE LA INDUSTRIA QUIMICAS Y PETROQUIMICAS DE LA REPUBLICA ARGENTINA'));



               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socworks');
    }
}
