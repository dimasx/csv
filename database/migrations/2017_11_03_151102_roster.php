<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Roster extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            //Crear Tabla
            Schema::create('roster', function (Blueprint $table) {
            //Autoincrementable
            $table->increments('idn');
            //Datos especificos de tabla
            $table->string('name');
            $table->string('description');
            $table->integer('idnvacationtype');
            $table->integer('idncalendar');
            $table->integer('idnsalarytype');
            $table->integer('numperiodmonth');
            $table->integer('journaltype');
            $table->integer('journalteoric');
            //Datos por default en todas las tablas
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);  
            $table->timestamps();
        });
               DB::table('roster')
                        ->insert(array( 'name' => 'DIA24',
                                        'description'=>'Jornada Parcial Medio Tiempo',
                                        'idnvacationtype'=>1,
                                        'idncalendar'=>1,
                                        'idnsalarytype'=>1,
                                        'numperiodmonth'=>1,
                                        'journaltype'=>24,
                                        'journalteoric'=>8
                                       ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('roster');
    }
}
