<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Subperiod extends Migration
{
      /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('subperiod', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
            $table->integer('lock')->default(1); 
            $table->integer('active')->default(1);         
            $table->timestamps();
        });
            DB::table('subperiod')
                        ->insert(array(
                            'name' => 'subperiodo1',
                            'description' => 'subperiodo1'
                            ));
                      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('subperiod');
    }
}
