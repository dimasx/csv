<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Employeecondition extends Migration
{
      /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('employeecondition', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

                DB::table('employeecondition')
                ->insert(array('name' => 'Jubilado Decreto Nro 894/01 y/o Dec 2288/02',
                            'description' => 'Jubilado Decreto Nro 894/01 y/o Dec 2288/02'));
                DB::table('employeecondition')
                ->insert(array('name' => 'Menor',
                            'description' => 'Menor'));
                DB::table('employeecondition')
                ->insert(array('name' => 'SERVICIOS DIFERENCIADOS Mayor 18 años',
                            'description' => 'SERVICIOS DIFERENCIADOS Mayor 18 años'));
                DB::table('employeecondition')
                ->insert(array('name' => 'Pre- jubilables Sin relación de dependencia –Sin servicios reales',
                            'description' => 'Pre- jubilables Sin relación de dependencia –Sin servicios reales'));
                DB::table('employeecondition')
                ->insert(array('name' => 'Jubilado Decreto Nro 206/00 y/o Decreto Nro 894/01',
                            'description' => 'Jubilado Decreto Nro 206/00 y/o Decreto Nro 894/01'));
                DB::table('employeecondition')
                ->insert(array('name' => 'Pensión (NO SIPA)',
                            'description' => 'Pensión (NO SIPA)'));
                DB::table('employeecondition')
                ->insert(array('name' => 'Pensión no Contributiva (NO SIPA)',
                            'description' => 'Pensión no Contributiva (NO SIPA)'));
                DB::table('employeecondition')
                ->insert(array('name' => 'SERVICIOS COMUNES Mayor 18 años',
                            'description' => 'SERVICIOS COMUNES Mayor 18 años'));
                DB::table('employeecondition')
                ->insert(array('name' => 'Jubilado',
                            'description' => 'Jubilado'));


               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employeecondition');
    }
}
