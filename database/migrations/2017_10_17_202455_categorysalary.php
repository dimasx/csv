<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Categorysalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('categorysalary', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->integer('idncct');
            $table->float('salaryamount');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

         DB::table('categorysalary')
                ->insert(array('name' => 'ejecutivo',
                            'idncct' => 1,
                            'salaryamount' => 1500));

               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorysalary');
    }
}
