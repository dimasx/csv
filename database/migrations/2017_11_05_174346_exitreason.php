<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Exitreason extends Migration
{
      /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('exitreason', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

         DB::table('exitreason')
                ->insert(array('name' => 'Jubilación',
                            'description' => 'Jubilación'));

 DB::table('exitreason')
                ->insert(array('name' => 'Despido con causa',
                            'description' => 'Despido con causa'));

 DB::table('exitreason')
                ->insert(array('name' => 'Despido sin causa',
                            'description' => 'Despido sin causa'));

 DB::table('exitreason')
                ->insert(array('name' => 'Renuncia',
                            'description' => 'Renuncia'));

 DB::table('exitreason')
                ->insert(array('name' => 'Fallecimiento',
                            'description' => 'Fallecimiento'));

               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exitreason');
    }
}
