<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Positionjob extends Migration
{
       /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('positionjob', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

         DB::table('positionjob')
                ->insert(array('name' => 'Jefe',
                            'description' => 'Prueba'));

               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('positionjob');
    }
}
