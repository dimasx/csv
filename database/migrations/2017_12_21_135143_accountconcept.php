<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Accountconcept extends Migration
{
       /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
            //Crear Tabla
            Schema::create('accountconcept', function (Blueprint $table) {
            //Autoincrementable
            $table->increments('idn');
            $table->integer('idnconcept');
            $table->integer('idnaccountingaccount');
            //Datos por default en todas las tablas
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);  
            $table->timestamps();
        });
               DB::table('accountconcept')
                        ->insert(array( 
                            'idnconcept' => 1,
                            'idnaccountingaccount' => 1
                                                     
                                       ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('accountconcept');
    }
}
