<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Levelstudies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('levelstudies', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });


                DB::table('levelstudies')
                ->insert(array('name' => 'Primario Incompleto',
                            'description' => 'Primario Incompleto'));
                DB::table('levelstudies')
                ->insert(array('name' => 'Secundario Incompleto',
                            'description' => 'Secundario Incompleto'));
                DB::table('levelstudies')
                ->insert(array('name' => 'Secundario en Curso',
                            'description' => 'Secundario en Curso'));
                DB::table('levelstudies')
                ->insert(array('name' => 'Secundario Completo',
                            'description' => 'Secundario Completo'));
                DB::table('levelstudies')
                ->insert(array('name' => 'Terciario Incompleto',
                            'description' => 'Terciario Incompleto'));
                DB::table('levelstudies')
                ->insert(array('name' => 'Terciario en Curso',
                            'description' => 'Terciario en Curso'));
                DB::table('levelstudies')
                ->insert(array('name' => 'Universitario Incompleto',
                            'description' => ' Universitario Incompleto'));
                DB::table('levelstudies')
                ->insert(array('name' => 'Universitario en Curso',
                            'description' => 'Universitario en Curso'));
                DB::table('levelstudies')
                ->insert(array('name' => 'Universitario Completo',
                            'description' => 'Universitario Completo'));
                DB::table('levelstudies')
                ->insert(array('name' => 'Doctorado',
                            'description' => 'Doctorado'));
                DB::table('levelstudies')
                ->insert(array('name' => 'MBA',
                            'description' => 'MBA'));
                DB::table('levelstudies')
                ->insert(array('name' => 'Master',
                            'description' => 'Master'));
                DB::table('levelstudies')
                ->insert(array('name' => 'Postgrado',
                            'description' => 'Postgrado'));

               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('levelstudies');
    }
}
