<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeJobPositions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('employeejobpositions', function (Blueprint $table) {
            $table->increments('idn');
            $table->date('startdate');
            $table->date('finishdate');
            $table->integer('idnpositionjob');
            $table->integer('idncategorysalary');
            $table->integer('idncostcenter');
            $table->integer('idnemployee');
            $table->integer('lock')->default(1); 
            $table->integer('active')->default(1);         
            $table->timestamps();
        });
            DB::table('employeejobpositions')
                        ->insert(array(
                            'startdate' => '2017-12-12',
                            'finishdate' => '2018-08-12',
                            'idnpositionjob' => 1,
                            'idncategorysalary' => 1,
                            'idncostcenter' => 1,
                            'idnemployee' => 1
                            ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employeejobpositions');
    }
}
