<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Period extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('period', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('title');
            $table->integer('idncalendar');
            $table->integer('idntypeperiod');
            $table->date('startdate');
            $table->date('finishdate');
            $table->integer('year');
            $table->integer('month');
            $table->integer('sicossactive');
            $table->integer('special');
            $table->integer('numperiod');
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);  
            $table->timestamps();
        });
               DB::table('period')
                        ->insert(array('title' => 'Agosto 2017',
                                        'idncalendar'=>'1',
                                       'idntypeperiod'=>'1',
                                        'startdate'=>'2017-08-01',
                                        'finishdate'=>'2017-08-05',
                                        'year'=>'2017',
                                        'month'=>'10',
                                        'sicossactive'=>'1',
                                        'special'=>'0',
                                        'numperiod'=>'1'));


                DB::table('period')
                        ->insert(array('title' => 'Septiembre 2017',
                                        'idncalendar'=>'1',
                                       'idntypeperiod'=>'1',
                                        'startdate'=>'2017-09-01',
                                        'finishdate'=>'2017-09-10',
                                        'year'=>'2017',
                                        'month'=>'09',
                                        'sicossactive'=>'1',
                                        'special'=>'0',
                                        'numperiod'=>'2'));

                            

                DB::table('period')
                        ->insert(array('title' => 'Octubre 2017',
                                        'idncalendar'=>'1',
                                       'idntypeperiod'=>'1',
                                        'startdate'=>'2017-10-01',
                                        'finishdate'=>'2017-10-10',
                                        'year'=>'2017',
                                        'month'=>'10',
                                        'sicossactive'=>'1',
                                        'special'=>'0',
                                        'numperiod'=>'2'));




                DB::table('period')
                        ->insert(array('title' => 'Noviembre 2017',
                                        'idncalendar'=>'1',
                                       'idntypeperiod'=>'1',
                                        'startdate'=>'2017-11-01',
                                        'finishdate'=>'2017-11-10',
                                        'year'=>'2017',
                                        'month'=>'11',
                                        'sicossactive'=>'1',
                                        'special'=>'1',
                                        'numperiod'=>'2'));



                DB::table('period')
                        ->insert(array('title' => 'Diciembre 2017',
                                        'idncalendar'=>'1',
                                       'idntypeperiod'=>'1',
                                        'startdate'=>'2017-09-01',
                                        'finishdate'=>'2017-09-10',
                                        'year'=>'2017',
                                        'month'=>'12',
                                        'sicossactive'=>'1',
                                        'special'=>'0',
                                        'numperiod'=>'2'));
                       
                                            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('period');
    }
}
