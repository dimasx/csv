<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Civilstatus extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('civilstatus', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('name')->unique();
            $table->string('description');
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

        DB::table('civilstatus')
                ->insert(array('name' => 'Soltero',
                            'description' => 'Soltero'));
        DB::table('civilstatus')
                ->insert(array('name' => 'Casado',
                            'description' => 'Casado'));
        DB::table('civilstatus')
                ->insert(array('name' => 'Divorciado',
                            'description' => 'Divorciado'));
        DB::table('civilstatus')
                ->insert(array('name' => 'Viudo',
                            'description' => 'Viudo'));
        DB::table('civilstatus')
                ->insert(array('name' => 'Separado',
                            'description' => 'Separado'));
        DB::table('civilstatus')
                ->insert(array('name' => 'Concubinato',
                            'description' => 'Concubinato'));

               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('civilstatus');
    }
}
