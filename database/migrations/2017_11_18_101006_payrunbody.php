<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class payrunbody extends Migration
{
   
    
     public function up()
    {
        Schema::create('payrunbody', function (Blueprint $table) {
            $table->increments('idn');
            $table->integer('idnpayrunhead');
            $table->integer('idnconcept');
            $table->float('cant');
            $table->float('amount');
            
             $table->integer('lock')->default(1);
              $table->integer('active')->default(1);
            $table->timestamps();
        });

        DB::table('payrunbody')
                ->insert(array('idnpayrunhead' => '1',
                                'idnconcept' => '1',
                                'cant' => '5',
                               'amount' => '13231'
                               
                              ));

               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrunbody');
    }
    
    
    
    
    
    
}
